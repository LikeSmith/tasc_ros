#!/usr/bin/env python2
'''
mfrmfp_robcomm.py

This ROS node recieves marker positions from ARToolKit Server
'''

import rospy
import socket
import math
import tf
import numpy as np
from tasc_ros.msg import MFRMFPNodeState, MFRMFPParentState
from std_msgs.msg import Int8
from multiprocessing import Process, Queue

class Marker_Datpt:
  def __init__(self, ind, time, quat, disp):
    self.i = ind
    self.t = time
    self.q = quat
    self.d = disp

  def rot(self, v):
    q = self.q[1:4, 0]
    q0 = self.q[0, 0]
    v_r = (q0**2 - (np.transpose(q)*q)[0, 0])*v + 2*q0*np.transpose(np.matrix(np.cross(np.transpose(q), np.transpose(v)))) + 2*q*np.transpose(q)*v
    return v_r

  def rot_inv(self, v):
    q = -self.q[1:4, 0]
    q0 = self.q[0, 0]
    v_r = (q0**2 - (np.transpose(q)*q)[0, 0])*v + 2*q0*np.transpose(np.matrix(np.cross(np.transpose(q), np.transpose(v)))) + 2*q*np.transpose(q)*v
    return v_r

  def tostr(self):
    s = '%d,%d,%f,%f,%f,%f,%f,%f,%f'%(self.i, self.t, self.q[0, 0], self.q[1, 0], self.q[2, 0], self.q[3, 0], self.d[0, 0], self.d[1, 0], self.d[2, ])
    return s

def cal_sig_callback(msg, q):
    q.put(1)

def reciever(conn, q, k, N):
    running = True
    timeout_count = 1
    markers = []

    conn.settimeout(1.0)

    for i in range(N):
        marker = Marker_Datpt(i, -1, np.mat([[1], [0], [0], [0]]), np.mat([[0], [0], [0]]))
        markers.append(marker)

    while running:
        if k.qsize() > 0:
            if k.get() == 'kill':
                running = False
                break

        try:
            data = conn.recv(1)
        except socket.timeout:
            print 'Socket timed out (%d%)'%timeout_count
            timeout_count += 1

            if timeout_count > 10:
                print 'Killing...'
                running = False
                break
            else:
                continue

        if data == 'P':
            data = conn.recv(1)
            pktlen = ''
            while data != ';':
              pktlen += data
              data = conn.recv(1)
            l = int(pktlen)
        else:
            continue

        data = conn.recv(l)

        while len(data) < l:
            new_dat = conn.recv(l - len(data))
            data += new_dat

        if data[0] == 'E':
            q.put([markers, float(data[1:])/1000.0])
        elif data[0] == 'M':
            try:
                data = data[1:]
                index = int(data.split(',')[0])
                t = float(data.split(',')[1])/1000.0
                quat = np.mat([[float(data.split(',')[2])], [float(data.split(',')[3])], [float(data.split(',')[4])], [float(data.split(',')[5])]])
                dis = np.mat([[float(data.split(',')[6])], [float(data.split(',')[7])], [float(data.split(',')[8])]])/1000.0
            except ValueError:
                print 'ValueError, continuing...'
                continue

            markers[index] = Marker_Datpt(index, t, quat, dis)
            #print 'M%d pos: [%f, %f, %f]'%(index, markers[index].d[0, 0], markers[index].d[1, 0], markers[index].d[2, 0])

        else:
            print 'Recieved bad packet: %s.'%(data)

if __name__ == '__main__':
    rospy.init_node('mfrmfp_robcomm')

    cal = False
    cal_norm = np.mat([[0], [0], [-1]])
    norm = np.mat([[0], [0], [-1]])
    cal_coeffs = np.mat([[0], [0], [0]])
    coeffs = np.mat([[0], [0], [0]])
    theta_last = 0
    t_last = 0

    # load parameters
    try:
        robs_i = rospy.get_param('/robot_marker_indicies')
        plane_i = rospy.get_param('/plane_marker_indicies')
        theta_win_size = rospy.get_param('/theta_win_size')
        pos_win_size = rospy.get_param('/pos_win_size')
        axis_i = rospy.get_param('/axis_marker_indicies')
        port = rospy.get_param('/ARTK_port')
    except KeyError:
        print 'Parameters not specified, exiting'
        exit()

    theta_win = np.zeros(theta_win_size)
    pos_win = []
    for i in range(len(robs_i)):
        pos_win.append([])
        for j in range(pos_win_size):
            pos_win[i].append(0.0)

    # set up Things
    print 'Setting up ROS'

    parent_state_pub = rospy.Publisher('/TASC/Parent/x', MFRMFPParentState, queue_size=1)
    swarm_state_pub = []

    calib_q = Queue()
    rospy.Subscriber('/TASC/Calib', Int8, cal_sig_callback, calib_q, queue_size=1)

    for i in range(len(robs_i)):
        swarm_state_pub.append(rospy.Publisher('/TASC/Swarm/rob%d/x'%i, MFRMFPNodeState, queue_size=1))

    print 'Opening socket on port %d'%port

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('0.0.0.0', port))

    print 'listening for incomming connections'
    s.listen(1)
    conn, addr = s.accept()

    print 'Connection accepted from ', addr

    # start listener
    N = len(robs_i)+len(plane_i);

    q = Queue()
    k = Queue()
    print 'starting reciever for %d markers'%N
    p = Process(target=reciever, args=(conn, q, k, N))

    p.start()

    # execute main loop
    while not rospy.is_shutdown():
        if calib_q.qsize() > 0:
            req = calib_q.get()
            cal = True

        while q.qsize() > 1:
            data = q.get()

        if q.qsize() == 1:
            data = q.get()

            markers = data[0]
            t = data[1]

            if cal:
                M = np.mat(np.zeros((len(plane_i), 3)))
                v = np.mat(np.zeros((len(plane_i), 1)))

                for i in range(len(plane_i)):
                    #if abs(t - markers[plane_i[i]].t) < 0.5:
                    if markers[plane_i[i]].t > 0:
                        M[i, 0] = markers[plane_i[i]].d[0, 0]
                        M[i, 1] = markers[plane_i[i]].d[1, 0]
                        M[i, 2] = -1.0
                        v[i, 0] = -markers[plane_i[i]].d[2, 0]
                    else:
                        print 'Not all plane markers visible. (t = %f, t_%d = %f)'%(t, plane_i[i], markers[plane_i[i]].t)
                        cal = False

                if cal:
                    cal = False
                    cal_coeffs = np.linalg.pinv(M)*v
                    cal_norm = np.mat([[coeffs[0, 0]], [coeffs[1, 0]], [1.0]])
                    cal_norm = cal_norm / np.linalg.norm(cal_norm)
                    theta_last = 0
                    t_last = t
                    print 'Calibration Successful!'
                    print 'Calibrated norm: [%f, %f, %f]'%(cal_norm[0, 0], cal_norm[1, 0], cal_norm[2, 0])
                    continue

            plane_markers_seen = []

            for i in plane_i:
                if t == markers[i].t:
                    plane_markers_seen.append(i)

            if len(plane_markers_seen) < 3:
                print 'Not enough visible markers!!'
                continue
            else:
                M = np.mat(np.zeros((len(plane_markers_seen), 3)))
                v = np.mat(np.zeros((len(plane_markers_seen), 1)))

                for i in range(len(plane_markers_seen)):
                    M[i, 0] = markers[plane_markers_seen[i]].d[0, 0]
                    M[i, 1] = markers[plane_markers_seen[i]].d[1, 0]
                    M[i, 2] = -1.0
                    v[i, 0] = -markers[plane_markers_seen[i]].d[2, 0]

                for i in range(theta_win_size-1):
                    theta_win[i] = theta_win[i+1];

                coeffs = np.linalg.pinv(M)*v
                norm = np.mat([[coeffs[0, 0]], [coeffs[1, 0]], [1.0]])
                norm = norm / np.linalg.norm(norm)

                try:
                    theta_win[-1] = math.acos(abs(np.transpose(norm)*cal_norm))
                except ValueError:
                    if abs(np.transpose(norm)*cal_norm - 1.0 < 0.01) and abs(np.transpose(norm)*cal_norm) > 0.0:
                        theta_win[-1] = math.acos(1.0)
                    else:
                        print 'Value Error'
                        print norm
                        print cal_norm

                v1 = np.transpose(np.cross(np.transpose(norm), np.transpose(cal_norm)))
                v2 = markers[axis_i[0]].d - markers[axis_i[1]].d

                if np.transpose(v1)*v2 < 0.0:
                    theta_win[-1] *= -1.0

                parent_state_msg = MFRMFPParentState()
                parent_state_msg.theta = np.median(theta_win)
                parent_state_msg.omega = (np.median(theta_win) - theta_last)/(t - t_last)
                parent_state_msg.t = t
                parent_state_pub.publish(parent_state_msg)
                theta_last = np.median(theta_win)
                t_last = t

                #p_0 = np.mat([[0], [0], [0]])
                #p_0[1, 0] = (coeffs[2, 0] - cal_coeffs[2, 0])/(coeffs[1, 0] - cal_coeffs[1, 0])
                #p_0[2, 0] = -coeffs[1, 0]*p_0[1, 0] + coeffs[2, 0]
                #a = np.cross(np.transpose(norm), np.transpose(cal_norm))
                p_axis1 = markers[axis_i[0]].d - (np.transpose(norm)*markers[axis_i[0]].d - coeffs[2, 0])[0, 0]*norm
                p_axis2 = markers[axis_i[1]].d - (np.transpose(norm)*markers[axis_i[1]].d - coeffs[2, 0])[0, 0]*norm

                a1 = p_axis1 - p_axis2
                a2 = np.mat(np.transpose(np.cross(np.transpose(a1), np.transpose(norm))))
                A = np.concatenate([a1, a2], axis=1)

                A_dagg = np.linalg.pinv(A)

                for i in range(len(robs_i)):
                    #if t == markers[robs_i[i]].t:
                    for j in range(pos_win_size-1):
                        pos_win[i][j] = pos_win[i][j+1]

                    pos = markers[robs_i[i]].d - (np.transpose(norm)*markers[robs_i[i]].d - coeffs[2, 0])[0, 0]*norm
                    p_plane = A_dagg*pos
                    pos_win[i][-1] = -p_plane[1, 0]

                    swarm_state_msg = MFRMFPNodeState()
                    swarm_state_msg.pos = np.median(pos_win[i])
                    swarm_state_msg.t = t
                    swarm_state_pub[i].publish(swarm_state_msg)

    k.put('kill')
    p.join();
