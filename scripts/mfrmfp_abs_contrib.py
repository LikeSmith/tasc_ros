#!/usr/bin/env python2
'''
mfrmfp_abs_contrib.py

calculates abstract contributions for a single robot node
'''

# set up to use python3 stuff
#from __future__ import print_function, unicode_literals
#from __future__ import absolute_import, division

# perform imports
import rospy
import numpy as np
from tasc_ros.msg import MFRMFPAbstractState2, MFRMFPNodeState2
import tasc_learning.models.tasc_modular as NN
import sys

def node_state_cbk(msg, args):
    a_cur = MFRMFPAbstractState2()
    a_aux = MFRMFPAbstractState2()

    x = np.array([msg.p, msg.v])

    a_cur.a = args[1].eval(x, args[0]).tolist()
    a_aux.a = args[2].eval(x, args[0]).tolist()

    a_cur.t = msg.t
    a_aux.t = msg.t

    args[3].publish(a_cur)
    args[4].publish(a_aux)


def abs_contrib(i):
    rospy.init_node('abs_contrib%d'%i)

    # load parameters
    try:
        abs_contrib_param = rospy.get_param('/abs_contrib_param')
        abs_contrib_net = rospy.get_param('/abs_contrib_net')
        aux_contrib_param = rospy.get_param('/aux_contrib_param')
        aux_contrib_net = rospy.get_param('/aux_contrib_net')
        params = rospy.get_param('/rob%d_params'%i)
    except KeyError:
        rospy.logfatal('Parameters not specified, exiting')
        return

    abs_contrib_nn = NN.Abstract_Contrib(param_file=abs_contrib_param, net_file=abs_contrib_net)

    aux_contrib_nn = NN.Abstract_Contrib(param_file=aux_contrib_param, net_file=aux_contrib_net)

    abs_contrib_pub = rospy.publisher('/TASC/Swarm/rob%d/a_cur', MFRMFPAbstractState2, queue_size=1)

    aux_contrib_pub = rospy.publisher('/TASC/Swarm/rob%d/a_aux', MFRMFPAbstractState2, queue_size=1)

    rospy.Subscriber('/TASC/Swarm/rob%d/x'%i, MFRMFPNodeState2, node_state_cbk, (params, abs_contrib_nn, aux_contrib_nn, abs_contrib_pub, aux_contrib_pub), queue_size=1)

    rospy.loginfo('rob %d now calculating abstract contributions.')

    rospy.spin()

if __name__ == '__main__':
    if len(sys.argv) < 2:
        rospy.logfatal('specify node index')
        exit(1)
    try:
        abs_contrib(int(sys.argv[1]))
    except rospy.ROSInterruptException:
        pass
