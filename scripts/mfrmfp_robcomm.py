#!/usr/bin/env python2
'''
mfrmfp_robcomm.py

This ROS node forwards velocity commands to the 3Pi robots
'''

import rospy
import serial
import struct
from tasc_ros.msg import MFRMFPNodeInput
from multiprocessing import Process, Queue

class tpidata:
  "'connect to xbee and handle data'"
  #class vars
  reqdat = bytearray('\x7e\x00\x06\x01\x00\x00\x03\x00\x03\xf8')
  def __init__(self):
    self.buf = []
    self.recvd = []
    self.dat = []
    self.frame = []
    self.send = []
    self.i = 0
    self.j = 0
    self.k = 0

  def transmit(self,addr,lvel,rvel):
    self.delim = 0x7E #bytearray('\x7e')
    self.api = [0x01,0x00] #bytearray('\x01\x00')
    self.addh = 0x00 #bytearray('\x00')
    self.addl = addr #bytearray(addr)
    self.options = 0x00#bytearray('\x00')
    self.send = []
    self.sendsum = 0

    self.temp = struct.pack('f',lvel)
    self.send += struct.unpack('4B',self.temp)
    self.temp =[]
    self.temp = struct.pack('f',rvel)
    self.send += struct.unpack('4B',self.temp)
    self.temp =[]

    self.sendsum = self.send[0]+self.send[1]+self.send[2]+self.send[3]+self.send[4]+self.send[5]+self.send[6]+self.send[7]

    self.checksum = 0xFF - (self.api[0]+self.api[1]+self.addh+self.addl+self.options+self.sendsum+1) & 0b11111111
    self.length = 1+2+1+1+1+len(self.send)

    self.written = 0

    self.frame = struct.pack('9B',self.delim,0,self.length,self.api[0],self.api[1],self.addh,self.addl,self.options,1)
    self.frame += struct.pack('B'*len(self.send),*self.send)
    self.frame += struct.pack('B',self.checksum)

    return self.frame

def sender(port, baud, q):
    xbee = serial.Serial(port, baud)
    running = True
    fudge_packer = tpidata()

    while(running):
        msg = q.get()

        if msg == 'kill':
            running = False
            break
        else:
            frame = fudge_packer.transmit(msg[0], msg[1]*1000.0, msg[1]*1000.0)
            xbee.write(frame)
            print 'sending vel=%f to robot %d at time [%f, %f]'%(msg[1], msg[0], msg[2], rospy.get_rostime().secs)


def callback(msg, args):
    args[0].put([args[1], msg.vel, msg.t])

def robcomm():
    rospy.init_node('mfrmfp_robcomm')

    # load parameters
    try:
        N = rospy.get_param('/num_robots')
        port = rospy.get_param('/xbee_port')
        baud = rospy.get_param('/xbee_baud')
        addr = []
        for i in range(N):
            addr.append(rospy.get_param('/xbee_addr%d'%i))
    except KeyError:
        print 'Parameters not specified, exiting'
        return

    print 'sending commands to %d robots over port %s with baud rate %d'%(N, port, baud)

    q = Queue()
    p = Process(target=sender, args=(port, baud, q))
    p.start()

    for i in range(N):
        rospy.Subscriber('/TASC/Swarm/rob%d/u'%i, MFRMFPNodeInput, callback, (q, addr[i]), queue_size=1)

    rospy.spin()

    q.put('kill')
    p.join()

if __name__ == '__main__':
    try:
        robcomm()
    except rospy.ROSInterruptException:
        pass
