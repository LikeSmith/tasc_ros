/*
 * mfrmfp_parent_controller_suprise.cpp
 *
 * This is a ros node that impliments a RISE controller assuming saturation on
 * the inputs for the parent system
 * in the TASC controller architecture.
 */

// ROS includes
#include <ros/ros.h>
#include <tasc_ros/MFRMFPParentState.h>
#include <tasc_ros/MFRMFPAbstractState.h>
#include <tasc_ros/aux_func.hpp>
#include <std_msgs/Int8.h>

// boost includes
#include <boost/bind.hpp>
#include <boost/function.hpp>

// standard includes
#include <math.h>
#include <vector>

// macros
#define GRAV 9.81

// structs
struct callback_params
{
  double alpha_1;
  double alpha_2;
  double alpha_3;
  double gamma_1;
  double gamma_2;
  double beta;
  double min_m;
  double len;
  int reset;
  ros::Publisher abs_des_pub;
  ros::Publisher abs_des_dot_pub;
  ros::Publisher parent_state_des_pub;
  boost::function<double(double)> des_traj;
  boost::function<double(double)> des_traj_d;
  boost::function<double(double)> des_traj_dd;
  boost::function<double(double)> des_traj_ddd;
  boost::function<double(double)> sign;
};

// function prototypes
void parent_state_callback(callback_params* params, const tasc_ros::MFRMFPParentState::ConstPtr& msg);
void init_cb(callback_params* params, const std_msgs::Int8::ConstPtr& msg);

// main function
int main(int argc, char** argv)
{
  double amp;
  double freq;
  double off;
  double phi;
  int sign = -1;
  callback_params cb_params;
  ros::Subscriber parent_state_sub;
  ros::Subscriber init_sub;

  // initilize ros
  ros::init(argc, argv, "mfrmfp_parent_controller_lqr");
  ros::NodeHandle n;

  // load parameters
  if (!ros::param::get("/parent_alpha_1", cb_params.alpha_1) || !ros::param::get("/parent_alpha_2", cb_params.alpha_2) || !ros::param::get("/parent_alpha_3", cb_params.alpha_3) || !ros::param::get("/parent_gamma_1", cb_params.gamma_1) || !ros::param::get("/parent_gamma_2", cb_params.gamma_2) || !ros::param::get("/parent_beta", cb_params.beta))
  {
    ROS_ERROR("Missing Parameter: SUPRISE gains");
    return 0;
  }

  ROS_INFO("SUPRISE Gains Loaded: alpha_1=%f alpha_2=%f alpha_3=%f gamma_1=%f gamma_2=%f beta=%f", cb_params.alpha_1, cb_params.alpha_2, cb_params.alpha_3, cb_params.gamma_1, cb_params.gamma_2, cb_params.beta);

  if (!ros::param::get("/min_m", cb_params.min_m) || !ros::param::get("/len", cb_params.len))
  {
    ROS_ERROR("Missing Parameter: physical properties");
    return 0;
  }

  ROS_INFO("Physical Properties of swarm and parent loaded: min_m=%f len=%f", cb_params.min_m, cb_params.len);

  if (!ros::param::get("/track_amp", amp) || !ros::param::get("/track_freq", freq) || !ros::param::get("/track_off", off) || !ros::param::get("/track_phi", phi))
  {
    ROS_ERROR("Missing Parameter: tracking trajectory");
    return 0;
  }

  ROS_INFO("Tracking trajectory parameters loaded: amp=%f freq=%f off=%f", amp, freq, off);

  ros::param::get("/sign", sign);

  switch (sign)
  {
  case 1:
    cb_params.sign = boost::bind(sgn, _1);
    ROS_INFO("Using signum function for sign");
    break;
  case 2:
    cb_params.sign = boost::bind(sat, _1);
    ROS_INFO("Using saturation function for sign");
    break;
  case 3:
    cb_params.sign = boost::bind(sgm, _1);
    ROS_INFO("Using sigmoid function for sign");
    break;
  default:
    cb_params.sign = boost::bind(sgn, _1);
    ROS_INFO("No sign function specified, using default signum function");
    break;
  }

  cb_params.des_traj = boost::bind(sineusoid, amp, freq, phi, off, _1);
  cb_params.des_traj_d = boost::bind(sineusoid, amp*2.0*PI*freq, freq, phi + PI/2.0, 0.0, _1);
  cb_params.des_traj_dd = boost::bind(sineusoid, amp*4.0*PI*PI*freq*freq, freq, phi - PI, 0.0, _1);
  cb_params.des_traj_ddd = boost::bind(sineusoid, amp*8.0*PI*PI*PI*freq*freq*freq, freq, phi - PI/2.0, 0.0, _1);
  cb_params.reset = 0;

  // setup publishers
  cb_params.abs_des_pub = n.advertise<tasc_ros::MFRMFPAbstractState>("/TASC/Swarm/abs_state_des", 1);
  cb_params.abs_des_dot_pub = n.advertise<tasc_ros::MFRMFPAbstractState>("/TASC/Swarm/abs_state_des_dot", 1);
  cb_params.parent_state_des_pub = n.advertise<tasc_ros::MFRMFPParentState>("/TASC/Parent/x_d", 1);

  // setup subscribers
  parent_state_sub = n.subscribe<tasc_ros::MFRMFPParentState>("/TASC/Parent/x", 1, boost::bind(parent_state_callback, &cb_params, _1));
  init_sub = n.subscribe<std_msgs::Int8>("/TASC/Sys/init", 1, boost::bind(init_cb, &cb_params, _1));

  // spin ros
  ros::spin();

  return 0;
}

// function definitions
void parent_state_callback(callback_params* params, const tasc_ros::MFRMFPParentState::ConstPtr& msg)
{
  static double t_last = 0.0;
  static double e2_last = 0.0;
  static double ef = 0.0;
  static double v = 0.0;

  tasc_ros::MFRMFPAbstractState a_d;
  tasc_ros::MFRMFPAbstractState a_d_dot;
  tasc_ros::MFRMFPParentState x_p_d;

  double e1;
  double e2;
  double r;
  double ef_dot;
  double v_dot;

  if (params->reset)
  {
    t_last = 0.0;
    e2_last = 0.0;
    ef = 0.0;
    v = 0.0;
    params->reset = 0;
  }

  x_p_d.theta = params->des_traj(msg->t);
  x_p_d.omega = params->des_traj_d(msg->t);
  x_p_d.t = msg->t;

  e1 = x_p_d.theta - msg->theta;
  e2 = x_p_d.omega - msg->omega + params->alpha_1*tanh(e1) + tanh(ef);
  r  = (e2 - e2_last)/(msg->t - t_last) + params->alpha_2*tanh(e2) + params->alpha_3*e2;

  ef_dot = pow(cosh(ef), 2)*(-params->gamma_1*e2 + tanh(e1) - params->gamma_2*tanh(ef));
  v_dot = pow(cosh(v), 2)*(params->alpha_2*tanh(e2) + params->alpha_3*e2 + params->beta*params->sign(e2) - params->alpha_1*e2/pow(cosh(e1), 2) + params->gamma_2*e2);

  a_d.tau = -params->gamma_1*tanh(v);
  a_d.J = params->min_m*params->len*params->len/4;
  a_d.t = msg->t;

  a_d_dot.tau = -params->gamma_1*v_dot*(1 - pow(tanh(v), 2));
  a_d_dot.J = 0.0;
  a_d_dot.t = a_d.t;

  params->abs_des_pub.publish(a_d);
  params->abs_des_dot_pub.publish(a_d_dot);
  params->parent_state_des_pub.publish(x_p_d);

  ef += ef_dot*(msg->t - t_last);
  v += v_dot*(msg->t - t_last);
  
  t_last = msg->t;
  e2_last = e2;
}

void init_cb(callback_params* params, const std_msgs::Int8::ConstPtr& msg)
{
  params->reset = 1;
  ROS_INFO("Resetting (count = %d)", msg->data);
}
