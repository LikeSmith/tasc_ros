/*
 * mfrmfp_swarm_com.cpp
 *
 * This node reads velocity topics for robots in the swarm and sends the
 * via xbee.
 */

// ROS includes
#include <ros/ros.h>
#include <tasc_ros/MFRMFPNodeInput.h>

int main(int argc, char** argv)
{
  int des_count = 10;
  int count = 0;
  char topic[32];

  ros::init(argc, argv, "com_vel");
  ros::NodeHandle n;

  if (argc < 3)
  {
    ROS_ERROR("not enough arguments.  usage: %s <rob_num> <vel>", argv[0]);
    exit(0);
  }

  if (argc == 4)
  {
    des_count = atoi(argv[3]);
  }

  sprintf(topic, "/TASC/Swarm/rob%d/u", atoi(argv[1]));
  ros::Publisher pub = n.advertise<tasc_ros::MFRMFPNodeInput>(topic, 1);

  ros::Rate loop_rate(10);

  while(ros::ok() && count < des_count)
  {
    tasc_ros::MFRMFPNodeInput msg;

    msg.vel = atof(argv[2]);
    msg.t = count;

    pub.publish(msg);

    ros::spinOnce();
    loop_rate.sleep();

    count++;
  }
}
