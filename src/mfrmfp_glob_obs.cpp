/*
 * mfrmfp_glob_obs.cpp
 *
 * This node performes the global observation of the mfrmfp swarm, reporting the
 * abstract state of the system, as well as necessary sums for calculation of
 * the desired velocity for members of the swarm.
 */

// ROS includes
#include <ros/ros.h>
#include <tasc_ros/MFRMFPGlobObs.h>
#include <tasc_ros/MFRMFPAbstractState.h>
#include <tasc_ros/MFRMFPNodeState.h>

// BOOST includes
#include <boost/bind.hpp>

// standard includes
#include <stdio.h>
#include <vector>

// function prototyes
void on_pos_update(double* pos, char* update_flag, double* t, const tasc_ros::MFRMFPNodeState::ConstPtr& msg);

// main function
int main (int argc, char** argv)
{
  int num_robs;
  double t = 0.0;
  double* masses;
  double* pos;
  char* update_flags;
  ros::Subscriber* subs;

  // initilize ros
  ros::init(argc, argv, "mfrmfp_glob_obs");
  ros::NodeHandle n;

  // load parameters
  if (!ros::param::get("/num_robots", num_robs))
  {
    ROS_ERROR("Misisng Parameter: number of robots.");
    return 0;
  }

  masses = new double[num_robs];
  pos = new double[num_robs];
  update_flags = new char[num_robs];
  subs = new ros::Subscriber[num_robs];

  ROS_INFO("Running Global Observer on %d robots.", num_robs);

  for (int i = 0; i < num_robs; i++)
  {
    char param_name[8];
    sprintf(param_name, "/mass%d", i);

    if (!ros::param::get(param_name, masses[i]))
    {
      ROS_ERROR("Missing Parameter: mass for robot %d.", i);
      return 0;
    }

    pos[i] = 0.0;
    update_flags[i] = 0;

    ROS_INFO("Robot %d has mass of %f.", i, masses[i]);
  }

  // set up subscribers
  for (int i = 0; i < num_robs; i++)
  {
    char topic[32];
    sprintf(topic, "/TASC/Swarm/rob%d/x", i);
    subs[i] = n.subscribe<tasc_ros::MFRMFPNodeState>(topic, 1, boost::bind(on_pos_update, pos+i, update_flags+i, &t, _1));
  }

  // set up publisher
  ros::Publisher abs_pub = n.advertise<tasc_ros::MFRMFPAbstractState>("/TASC/Swarm/abs_state", 1);
  ros::Publisher obs_pub = n.advertise<tasc_ros::MFRMFPGlobObs>("/TASC/Swarm/global_obs", 1);

  // main loop
  while (ros::ok())
  {
    bool ready_update = true;

    // check if all robots have updated
    for (int i =0; i < num_robs; i++)
    {
      if (!update_flags[i])
      {
        ready_update = false;
        break;
      }
    }

    // make and publish observations if all robots have updated
    if (ready_update)
    {
      tasc_ros::MFRMFPAbstractState abs_msg;
      tasc_ros::MFRMFPGlobObs obs_msg;

      abs_msg.tau = 0.0;
      abs_msg.J = 0.0;
      abs_msg.t = t/num_robs;
      obs_msg.sum0 = 0.0;
      obs_msg.sum1 = 0.0;
      obs_msg.sum2 = 0.0;
      obs_msg.sum3 = 0.0;
      obs_msg.t = t/num_robs;

      for (int i = 0; i < num_robs; i++)
      {
        abs_msg.tau += masses[i]*pos[i];
        abs_msg.J += masses[i]*pos[i]*pos[i];
        obs_msg.sum0 += masses[i]*masses[i];
        obs_msg.sum1 += masses[i]*masses[i]*pos[i];
        obs_msg.sum2 += masses[i]*masses[i]*pos[i]*pos[i];

        for (int j = i+1; j < num_robs; j++)
        {
          obs_msg.sum3 += masses[i]*masses[i]*masses[j]*masses[j]*(pos[i] - pos[j])*(pos[i] - pos[j]);
        }

        update_flags[i] = 0;
      }

      t = 0.0;

      abs_pub.publish(abs_msg);
      obs_pub.publish(obs_msg);
    }

    // spin ros
    ros::spinOnce();
  }

  // deallocate dynamic memory
  delete[] masses;
  delete[] pos;
  delete[] subs;

  return 0;
}

// function definitions
void on_pos_update(double* pos, char* update_flag, double* t, const tasc_ros::MFRMFPNodeState::ConstPtr& msg)
{
  if (!*update_flag)
  {
    *pos = msg->pos;
    *t += msg->t;
    *update_flag = 1;
  }
}
