/*
 * Vicon transcriber
 *
 * This node takes a vicon tf and transcribes it's data to a csv
 * file
 */

// ros includes
#include <ros/ros.h>
#include <geometry_msgs/TransformStamped.h>

// std includes
#include <stdio.h>

// BOOST includes
#include <boost/bind.hpp>

// function prototypes
void msg_callback(const geometry_msgs::TransformStamped::ConstPtr& msg, FILE* fp);

int main(int argc, char**argv)
{
    char* topic;
    char* file_name;
    char node_name[32];
    ros::Subscriber sub;
    FILE* fp;

    // interperate command line arguments
    if (argc < 4)
    {
        ROS_FATAL("Command arguments missing");
        ROS_FATAL("usage: %s, <topic to listen to> <output file path> <index>", argv[0]);
    }

    topic = argv[1];
    file_name = argv[2];
    sprintf(node_name, "vicon_transcriber_%s", argv[3]);

    // initialize ros
    ros::init(argc, argv, node_name);
    ros::NodeHandle n;

    ROS_INFO("Subscribing to topic '%s', publishing to '%s'", topic, file_name);

    fp = fopen(file_name, "w+");
    sub = n.subscribe<geometry_msgs::TransformStamped>(topic, 100, boost::bind(msg_callback, _1, fp));

    ros::spin();

    fclose(fp);

    return 0;
}

void msg_callback(const geometry_msgs::TransformStamped::ConstPtr& msg, FILE* fp)
{
    fprintf(fp, "%f,", msg->header.stamp.toSec());
    fprintf(fp, "%f,", msg->transform.translation.x);
    fprintf(fp, "%f,", msg->transform.translation.y);
    fprintf(fp, "%f,", msg->transform.translation.z);
    fprintf(fp, "%f,", msg->transform.rotation.w);
    fprintf(fp, "%f,", msg->transform.rotation.x);
    fprintf(fp, "%f,", msg->transform.rotation.y);
    fprintf(fp, "%f\n", msg->transform.rotation.z);
}
