/*
 * mfrmfp_parent_controller_arise.cpp
 *
 * This is a ros node that impliments an ARISE controller for the parent system
 * in the TASC controller architecture.
 */

// ROS includes
#include <ros/ros.h>
#include <tasc_ros/MFRMFPParentState.h>
#include <tasc_ros/MFRMFPAbstractState.h>
#include <tasc_ros/aux_func.hpp>
#include <std_msgs/Int8.h>

// boost includes
#include <boost/bind.hpp>
#include <boost/function.hpp>

// standard includes
#include <math.h>
#include <vector>

// macros
#define GRAV 9.81

// structs
struct callback_params
{
  double ks;
  double alpha_1;
  double alpha_2;
  double beta;
  std::vector<double> Gamma;
  double gamma_2;
  double gamma_3;
  double gamma_5;
  double min_m;
  double len;
  double phi_0[4];
  int reset;
  ros::Publisher abs_des_pub;
  ros::Publisher abs_des_dot_pub;
  ros::Publisher parent_state_des_pub;
  boost::function<double(double)> des_traj;
  boost::function<double(double)> des_traj_d;
  boost::function<double(double)> des_traj_dd;
  boost::function<double(double)> des_traj_ddd;
  boost::function<double(double)> sign;
};

// function prototypes
void parent_state_callback(callback_params* params, const tasc_ros::MFRMFPParentState::ConstPtr& msg);
void init_cb(callback_params* params, const std_msgs::Int8::ConstPtr& msg);

// main function
int main(int argc, char** argv)
{
  double amp;
  double freq;
  double off;
  double phi;
  int sign = -1;
  callback_params cb_params;
  ros::Subscriber parent_state_sub;
  ros::Subscriber init_sub;

  // initilize ros
  ros::init(argc, argv, "mfrmfp_parent_controller_lqr");
  ros::NodeHandle n;

  // load parameters
  if (!ros::param::get("/parent_ks", cb_params.ks) || !ros::param::get("/parent_alpha_1", cb_params.alpha_1) || !ros::param::get("/parent_alpha_2", cb_params.alpha_2) || !ros::param::get("/parent_beta", cb_params.beta) || !ros::param::get("/parent_Gamma", cb_params.Gamma))
  {
    ROS_ERROR("Missing Parameter: ARISE gains");
    return 0;
  }

  ROS_INFO("ARISE Gains Loaded: ks=%f alpha_1=%f alpha_2=%f beta=%f", cb_params.ks, cb_params.alpha_1, cb_params.alpha_2, cb_params.beta);
  ROS_INFO("ARISE adaptive gains loaded: Gamma=[%f %f %f %f; %f %f %f %f; %f %f %f %f; %f %f %f %f]", cb_params.Gamma[0], cb_params.Gamma[1], cb_params.Gamma[2], cb_params.Gamma[3], cb_params.Gamma[4], cb_params.Gamma[5], cb_params.Gamma[6], cb_params.Gamma[7], cb_params.Gamma[8], cb_params.Gamma[9], cb_params.Gamma[10], cb_params.Gamma[11], cb_params.Gamma[12], cb_params.Gamma[13], cb_params.Gamma[14], cb_params.Gamma[15]);

  if (!ros::param::get("/min_m", cb_params.min_m) || !ros::param::get("/len", cb_params.len) || !ros::param::get("/gamma_2", cb_params.gamma_2) || !ros::param::get("/gamma_3", cb_params.gamma_3) || !ros::param::get("/gamma_5", cb_params.gamma_5))
  {
    ROS_ERROR("Missing Parameter: physical properties");
    return 0;
  }

  ROS_INFO("Physical Properties of swarm and parent loaded: min_m=%f len=%f gamma_2=%f gamma_3=%f gamma_5=%f", cb_params.min_m, cb_params.len, cb_params.gamma_2, cb_params.gamma_3, cb_params.gamma_5);

  if (!ros::param::get("/track_amp", amp) || !ros::param::get("/track_freq", freq) || !ros::param::get("/track_off", off) || !ros::param::get("/track_phi", phi))
  {
    ROS_ERROR("Missing Parameter: tracking trajectory");
    return 0;
  }

  ROS_INFO("Tracking trajectory parameters loaded: amp=%f freq=%f off=%f", amp, freq, off);

  if (!ros::param::get("/phi1_0", cb_params.phi_0[0]) || !ros::param::get("/phi2_0", cb_params.phi_0[1]) || !ros::param::get("/phi3_0", cb_params.phi_0[2]) || !ros::param::get("/phi4_0", cb_params.phi_0[3]))
  {
    ROS_ERROR("Missing Parameter: initial estimates for adaptive terms");
  }

  ROS_INFO("Initial Estimates loaded for adaptive terms: phi=[%f %f %f %f]", cb_params.phi_0[0], cb_params.phi_0[1], cb_params.phi_0[2], cb_params.phi_0[3]);

  ros::param::get("/sign", sign);

  switch (sign)
  {
  case 1:
    cb_params.sign = boost::bind(sgn, _1);
    ROS_INFO("Using signum function for sign");
    break;
  case 2:
    cb_params.sign = boost::bind(sat, _1);
    ROS_INFO("Using saturation function for sign");
    break;
  case 3:
    cb_params.sign = boost::bind(sgm, _1);
    ROS_INFO("Using sigmoid function for sign");
    break;
  default:
    cb_params.sign = boost::bind(sgn, _1);
    ROS_INFO("No sign function specified, using default signum function");
    break;
  }

  cb_params.des_traj = boost::bind(sineusoid, amp, freq, phi, off, _1);
  cb_params.des_traj_d = boost::bind(sineusoid, amp*2.0*PI*freq, freq, phi + PI/2.0, 0.0, _1);
  cb_params.des_traj_dd = boost::bind(sineusoid, amp*4.0*PI*PI*freq*freq, freq, phi - PI, 0.0, _1);
  cb_params.des_traj_ddd = boost::bind(sineusoid, amp*8.0*PI*PI*PI*freq*freq*freq, freq, phi - PI/2.0, 0.0, _1);
  cb_params.reset = 0;

  // setup publishers
  cb_params.abs_des_pub = n.advertise<tasc_ros::MFRMFPAbstractState>("/TASC/Swarm/abs_state_des", 1);
  cb_params.abs_des_dot_pub = n.advertise<tasc_ros::MFRMFPAbstractState>("/TASC/Swarm/abs_state_des_dot", 1);
  cb_params.parent_state_des_pub = n.advertise<tasc_ros::MFRMFPParentState>("/TASC/Parent/x_d", 1);

  // setup subscribers
  parent_state_sub = n.subscribe<tasc_ros::MFRMFPParentState>("/TASC/Parent/x", 1, boost::bind(parent_state_callback, &cb_params, _1));
  init_sub = n.subscribe<std_msgs::Int8>("/TASC/Sys/init", 1, boost::bind(init_cb, &cb_params, _1));

  // spin ros
  ros::spin();

  return 0;
}

// function definitions
void parent_state_callback(callback_params* params, const tasc_ros::MFRMFPParentState::ConstPtr& msg)
{
  static double t_last = 0.0;
  static double e2_last = 0.0;
  static double e2_0 = 0.0;
  static double phi[]={params->phi_0[0], params->phi_0[1], params->phi_0[2], params->phi_0[3]};
  static double Lambda = 0.0;

  tasc_ros::MFRMFPAbstractState a_d;
  tasc_ros::MFRMFPAbstractState a_d_dot;
  tasc_ros::MFRMFPParentState x_p_d;

  double e1;
  double e2;
  double r;
  double Yd[4];
  double Yd_d[4];
  double Lambda_d;
  double phi_d[4];

  if (params->reset)
  {
    t_last = 0.0;
    e2_last = 0.0;
    phi[0] = params->phi_0[0];
    phi[1] = params->phi_0[1];
    phi[2] = params->phi_0[2];
    phi[3] = params->phi_0[3];
    Lambda = 0.0;
  }

  Yd[0] = params->des_traj_dd(msg->t);
  Yd[1] = tanh(params->gamma_2*params->des_traj_d(msg->t)) - tanh(params->gamma_3*params->des_traj_d(msg->t));
  Yd[2] = tanh(params->gamma_5*params->des_traj_d(msg->t));
  Yd[3] = params->des_traj_d(msg->t);

  Yd_d[0] = params->des_traj_ddd(msg->t);
  Yd_d[1] = params->gamma_2*params->des_traj_dd(msg->t)*(1 - pow(tanh(params->gamma_2*params->des_traj_d(msg->t)), 2)) - params->gamma_3*params->des_traj_dd(msg->t)*(1 - pow(tanh(params->gamma_3*params->des_traj_d(msg->t)), 2));
  Yd_d[2] = params->gamma_5*params->des_traj_dd(msg->t)*(1 - pow(tanh(params->gamma_2*params->des_traj_d(msg->t)), 2));
  Yd_d[3] = params->des_traj_dd(msg->t);

  x_p_d.theta = params->des_traj(msg->t);
  x_p_d.omega = params->des_traj_d(msg->t);
  x_p_d.t = msg->t;

  e1 = x_p_d.theta - msg->theta;
  e2 = x_p_d.omega - msg->omega + params->alpha_1*e1;
  r  = (e2 - e2_last)/(msg->t - t_last) + params->alpha_2*e2;

  if (params->reset)
  {
    e2_0 = e2;
    params->reset = 0;
  }

  a_d.tau = (Yd[0]*phi[0] + Yd[1]*phi[1] + Yd[2]*phi[2] + Yd[3]*phi[3] + (params->ks + 1)*(e2 - e2_0) + Lambda)/(GRAV*cos(msg->theta));
  a_d.J = params->min_m*params->len*params->len/4;
  a_d.t = msg->t;

  Lambda_d = (params->ks + 1)*params->alpha_2*e2 + params->beta*params->sign(e2);

  for (int i = 0; i < 4; i++)
  {
    phi_d[i] = 0.0;
    for (int j = 0; j < 4; j++)
    {
      phi_d[i] += r*Yd_d[j]*params->Gamma[i*4+j];
    }
  }

  a_d_dot.tau = ((Yd[0]*phi_d[0] + Yd[1]*phi_d[1] + Yd[2]*phi_d[2] + Yd_d[3]*phi_d[3] + Yd_d[0]*phi[0] + Yd_d[1]*phi[1] + Yd_d[2]*phi[2] + Yd_d[3]*phi[3] + (params->ks + 1)*r + params->beta*params->sign(e2))*GRAV*cos(msg->theta) + (Yd[0]*phi[0] + Yd[1]*phi[1] + Yd[2]*phi[2] + Yd[3]*phi[3] + (params->ks + 1)*(e2 - e2_0) + Lambda)*GRAV*msg->omega*sin(msg->theta))/pow(GRAV*cos(msg->theta), 2);
  a_d_dot.J = 0.0;
  a_d_dot.t = a_d.t;

  params->abs_des_pub.publish(a_d);
  params->abs_des_dot_pub.publish(a_d_dot);
  params->parent_state_des_pub.publish(x_p_d);

  Lambda += Lambda_d*(msg->t - t_last);
  for (int i = 0; i < 4; i++)
  {
    phi[i] += phi_d[i]*(msg->t - t_last);
  }

  t_last = msg->t;
  e2_last = e2;
}

void init_cb(callback_params* params, const std_msgs::Int8::ConstPtr& msg)
{
  params->reset = 1;
  ROS_INFO("Resetting (count = %d)", msg->data);
}
