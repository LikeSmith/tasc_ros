/*
 * mfrmfp_logger.cpp
 *
 * This ros node saves all topics to a series of csv files.
 */

// ROS includes
#include <ros/ros.h>
#include <tasc_ros/MFRMFPAbstractState.h>
#include <tasc_ros/MFRMFPGlobObs.h>
#include <tasc_ros/MFRMFPNodeInput.h>
#include <tasc_ros/MFRMFPNodeState.h>
#include <tasc_ros/MFRMFPParentState.h>
#include <std_msgs/Int8.h>

// bost includes
#include <boost/bind.hpp>

// standard includes
#include <stdio.h>
#include <fstream>

void abs_state_cb(std::ofstream* file, const tasc_ros::MFRMFPAbstractState::ConstPtr& msg);
void global_obs_cb(std::ofstream* file, const tasc_ros::MFRMFPGlobObs::ConstPtr& msg);
void node_input_cb(std::ofstream* file, const tasc_ros::MFRMFPNodeInput::ConstPtr& msg);
void node_state_cb(std::ofstream* file, const tasc_ros::MFRMFPNodeState::ConstPtr& msg);
void parent_state_cb(std::ofstream* file, const tasc_ros::MFRMFPParentState::ConstPtr& msg);
void reset_cb(std::ofstream** files, int n_robs, char* loc, const std_msgs::Int8::ConstPtr& msg);

int main(int argc, char** argv)
{
  int num_robs;
  char file_path[128];
  ros::Subscriber parent_state_sub;
  ros::Subscriber parent_state_des_sub;
  ros::Subscriber abs_state_sub;
  ros::Subscriber abs_state_des_sub;
  ros::Subscriber abs_state_des_dot_sub;
  ros::Subscriber global_obs_sub;
  ros::Subscriber* rob_states_sub;
  ros::Subscriber* rob_inputs_sub;
  ros::Subscriber* rob_vel_act_sub;
  ros::Subscriber init_sub;
  std::ofstream parent_state_out;
  std::ofstream parent_state_des_out;
  std::ofstream abs_state_out;
  std::ofstream abs_state_des_out;
  std::ofstream abs_state_des_dot_out;
  std::ofstream global_obs_out;
  std::ofstream* rob_states_out;
  std::ofstream* rob_inputs_out;
  std::ofstream* rob_vel_act_out;
  std::ofstream** all_files;

  // initilize ros
  ros::init(argc, argv, "mfrmfp_logger");
  ros::NodeHandle n;

  if (argc < 2)
  {
    ROS_ERROR("Missing destination directory.");
    return 0;
  }

  // Load parameters
  if (!ros::param::get("/num_robots", num_robs))
  {
    ROS_ERROR("Missing Parameter: number of robots");
    return 0;
  }

  all_files = new std::ofstream*[6+3*num_robs];

  ROS_INFO("Started logger for %d robots.  Saving data to [%s]", num_robs, argv[1]);

  // Open files
  sprintf(file_path, "%sparent_state.csv", argv[1]);
  parent_state_out.open(file_path, std::ofstream::out);
  all_files[0] = &parent_state_out;

  sprintf(file_path, "%sparent_state_des.csv", argv[1]);
  parent_state_des_out.open(file_path, std::ofstream::out);
  all_files[1] = &parent_state_des_out;

  sprintf(file_path, "%sabs_state.csv", argv[1]);
  abs_state_out.open(file_path, std::ofstream::out);
  all_files[2] = &abs_state_out;

  sprintf(file_path, "%sabs_state_des.csv", argv[1]);
  abs_state_des_out.open(file_path, std::ofstream::out);
  all_files[3] = &abs_state_des_out;

  sprintf(file_path, "%sabs_state_des_dot.csv", argv[1]);
  abs_state_des_dot_out.open(file_path, std::ofstream::out);
  all_files[4] = &abs_state_des_dot_out;

  sprintf(file_path, "%sglobal_obs.csv", argv[1]);
  global_obs_out.open(file_path, std::ofstream::out);
  all_files[5] = &global_obs_out;

  rob_states_out = new std::ofstream[num_robs];
  rob_inputs_out = new std::ofstream[num_robs];
  rob_vel_act_out = new std::ofstream[num_robs];

  for (int i = 0; i < num_robs; i++)
  {
    sprintf(file_path, "%srob%d_state.csv", argv[1], i);
    rob_states_out[i].open(file_path, std::ofstream::out);

    sprintf(file_path, "%srob%d_input.csv", argv[1], i);
    rob_inputs_out[i].open(file_path, std::ofstream::out);

    sprintf(file_path, "%srob%d_vel_act.csv", argv[1], i);
    rob_vel_act_out[i].open(file_path, std::ofstream::out);

    all_files[6+i*3] = &rob_states_out[i];
    all_files[7+i*3] = &rob_inputs_out[i];
    all_files[8+i*3] = &rob_vel_act_out[i];
  }

  ROS_INFO("Log files open, begining logging...");

  // setup subscribers
  parent_state_sub = n.subscribe<tasc_ros::MFRMFPParentState>("/TASC/Parent/x", 100, boost::bind(parent_state_cb, &parent_state_out, _1));
  parent_state_des_sub = n.subscribe<tasc_ros::MFRMFPParentState>("/TASC/Parent/x_d", 100, boost::bind(parent_state_cb, &parent_state_des_out, _1));
  abs_state_sub = n.subscribe<tasc_ros::MFRMFPAbstractState>("/TASC/Swarm/abs_state", 100, boost::bind(abs_state_cb, &abs_state_out, _1));
  abs_state_des_sub = n.subscribe<tasc_ros::MFRMFPAbstractState>("/TASC/Swarm/abs_state_des", 100, boost::bind(abs_state_cb, &abs_state_des_out, _1));
  abs_state_des_dot_sub = n.subscribe<tasc_ros::MFRMFPAbstractState>("/TASC/Swarm/abs_state_des_dot", 100, boost::bind(abs_state_cb, &abs_state_des_dot_out, _1));
  global_obs_sub = n.subscribe<tasc_ros::MFRMFPGlobObs>("/TASC/Swarm/global_obs", 100, boost::bind(global_obs_cb, &global_obs_out, _1));
  init_sub = n.subscribe<std_msgs::Int8>("/TASC/Sys/init", 100, boost::bind(reset_cb, all_files, num_robs, argv[1], _1));

  rob_states_sub = new ros::Subscriber[num_robs];
  rob_inputs_sub = new ros::Subscriber[num_robs];
  rob_vel_act_sub = new ros::Subscriber[num_robs];

  for (int i = 0; i < num_robs; i++)
  {
    char topic[32];

    sprintf(topic, "/TASC/Swarm/rob%d/x", i);
    rob_states_sub[i] = n.subscribe<tasc_ros::MFRMFPNodeState>(topic, 1, boost::bind(node_state_cb, &rob_states_out[i], _1));

    sprintf(topic, "/TASC/Swarm/rob%d/u", i);
    rob_inputs_sub[i] = n.subscribe<tasc_ros::MFRMFPNodeInput>(topic, 1, boost::bind(node_input_cb, &rob_inputs_out[i], _1));

    sprintf(topic, "/TASC/Swarm/rob%d/u_act", i);
    rob_vel_act_sub[i] = n.subscribe<tasc_ros::MFRMFPNodeInput>(topic, 1, boost::bind(node_input_cb, &rob_vel_act_out[i], _1));
  }

  // spin ros
  ros::spin();

  // close files;
  parent_state_out.close();
  parent_state_des_out.close();
  abs_state_out.close();
  abs_state_des_out.close();
  abs_state_des_dot_out.close();
  global_obs_out.close();

  for (int i = 0; i < num_robs; i++)
  {
    rob_states_out[i].close();
    rob_inputs_out[i].close();
    rob_vel_act_out[i].close();
  }

  // release dynamic memory;
  delete[] rob_states_sub;
  delete[] rob_inputs_sub;
  delete[] rob_vel_act_sub;
  delete[] rob_states_out;
  delete[] rob_inputs_out;
  delete[] rob_vel_act_out;
  delete[] all_files;

  // exit program
  return 0;
}

void abs_state_cb(std::ofstream* file, const tasc_ros::MFRMFPAbstractState::ConstPtr& msg)
{
  *file << msg->t << "," << msg->tau << "," << msg->J << std::endl;
}

void global_obs_cb(std::ofstream* file, const tasc_ros::MFRMFPGlobObs::ConstPtr& msg)
{
  *file << msg->t << "," << msg->sum0 << "," << msg->sum1 << "," << msg->sum2 << "," << msg->sum3 << std::endl;
}

void node_input_cb(std::ofstream* file, const tasc_ros::MFRMFPNodeInput::ConstPtr& msg)
{
  *file << msg->t << "," <<msg->vel << std::endl;
}

void node_state_cb(std::ofstream* file, const tasc_ros::MFRMFPNodeState::ConstPtr& msg)
{
  *file << msg->t << "," << msg->pos << std::endl;
}

void parent_state_cb(std::ofstream* file, const tasc_ros::MFRMFPParentState::ConstPtr& msg)
{
  *file << msg->t << "," << msg->theta << "," << msg->omega << std::endl;
}

void reset_cb(std::ofstream** files, int n_robs, char* loc, const std_msgs::Int8::ConstPtr& msg)
{
  const static int n = 5+3*n_robs;
  char file_path[128];

  for (int i = 0; i < n; i++)
  {
    files[i]->close();
  }

  sprintf(file_path, "%sparent_state.csv", loc);
  files[0]->open(file_path, std::ofstream::out);

  sprintf(file_path, "%sparent_state_des.csv", loc);
  files[1]->open(file_path, std::ofstream::out);

  sprintf(file_path, "%sabs_state.csv", loc);
  files[2]->open(file_path, std::ofstream::out);

  sprintf(file_path, "%sabs_state_des.csv", loc);
  files[3]->open(file_path, std::ofstream::out);

  sprintf(file_path, "%sabs_state_des_dot.csv", loc);
  files[4]->open(file_path, std::ofstream::out);

  sprintf(file_path, "%sglobal_obs.csv", loc);
  files[5]->open(file_path, std::ofstream::out);

  for (int i = 0; i <  n_robs; i++)
  {
    sprintf(file_path, "%srob%d_state.csv", loc, i);
    files[6+i*3]->open(file_path, std::ofstream::out);

    sprintf(file_path, "%srob%d_input.csv", loc, i);
    files[7+i*3]->open(file_path, std::ofstream::out);

    sprintf(file_path, "%srob%d_vel_act.csv", loc, i);
    files[8+i*3]->open(file_path, std::ofstream::out);
  }
}
