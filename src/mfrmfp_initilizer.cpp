/*
 * mfrmfp_initilize.cpp
 *
 * this node sets the initial contitions for the system.
 */

// ROS includes
#include <ros/ros.h>
#include <tasc_ros/MFRMFPParentState.h>
#include <tasc_ros/MFRMFPNodeState.h>
#include <std_msgs/Int8.h>

// main function
int main(int argc, char** argv)
{
  int num_robs;
  ros::Publisher* rob_pubs;
  ros::Publisher plane_pub;
  ros::Publisher init_pub;
  tasc_ros::MFRMFPNodeState* rob_state_msgs;
  tasc_ros::MFRMFPParentState plane_state_msg;
  std_msgs::Int8 init_msg;
  double tmp_theta;
  double tmp_omega;
  int count = 0;
  int des_count = 1;

  // initilize ROS
  ros::init(argc, argv, "mfrmfp_initilizer");
  ros::NodeHandle n;

  // Load parameters
  if (argc > 1)
  {
    des_count = atoi(argv[1]);
  }

  if (!ros::param::get("/num_robots", num_robs))
  {
    ROS_ERROR("Missing Parameter: number of robots not specified");
    return 0;
  }

  rob_pubs = new ros::Publisher[num_robs];
  rob_state_msgs = new tasc_ros::MFRMFPNodeState[num_robs];

  for (int i = 0; i < num_robs; i++)
  {
    char param_name[32];
    char topic_name[32];
    double tmp;
    sprintf(param_name, "/rob%d_p0", i);
    sprintf(topic_name, "TASC/Swarm/rob%d/x0", i);
    if (!ros::param::get(param_name, tmp))
    {
      ROS_ERROR("Missing Parameter: inital state for robot %d", i);
      return 0;
    }

    rob_pubs[i] = n.advertise<tasc_ros::MFRMFPNodeState>(topic_name, 1);
    rob_state_msgs[i].pos = tmp;
    rob_state_msgs[i].t = 0.0;
  }

  if (!ros::param::get("/theta0", tmp_theta) || !ros::param::get("/omega0", tmp_omega))
  {
    ROS_ERROR("Missing Parameter: plane initial state");
    return 0;
  }

  plane_pub = n.advertise<tasc_ros::MFRMFPParentState>("/TASC/Parent/x0", 1);
  plane_state_msg.theta = tmp_theta;
  plane_state_msg.omega = tmp_omega;
  plane_state_msg.t = 0.0;

  init_pub = n.advertise<std_msgs::Int8>("/TASC/Sys/init", 1);
  init_msg.data = count;

  ROS_INFO("Initial Positions Loaded, sending %d times. Press ctrl-c to close", des_count);

  ros::Rate loop(10);
  while(ros::ok() && count < des_count)
  {
    for (int i = 0; i < num_robs; i++)
    {
      rob_pubs[i].publish(rob_state_msgs[i]);
    }

    plane_pub.publish(plane_state_msg);

    init_msg.data=count++;
    init_pub.publish(init_msg);

    ros::spinOnce();
    loop.sleep();
  }

  delete[] rob_pubs;
  delete[] rob_state_msgs;

  return 0;
}
