/*
 * mfrmfp_vicon_listener.cpp
 *
 * This node is designed to interface with the vicon_bridge package
 * to generate the topics for running the mfrmfp experiment.  This
 * node will subscribe to the vicon topics and calculate the robot's
 * position on the plane as well as the plane's state.
 */

// ROS includes
#include <ros/ros.h>
#include <tasc_ros/MFRMFPNodeState.h>
#include <tasc_ros/MFRMFPParentState.h>
#include <tasc_ros/quat_func.hpp>
#include <tasc_ros/aux_func.hpp>
#include <tasc_ros/data_buffer.hpp>
#include <geometry_msgs/TransformStamped.h>

// BOOST includes
#include <boost/bind.hpp>

// standard includes
#include <string>
#include <vector>
#include <cmath>
#include <cstring>

// macros
#define LOOP_RATE 10

// function prototypes
void on_parent_update(Frame_Buffer* fb, const geometry_msgs::TransformStamped::ConstPtr& msg);
void on_rob_update(Frame_Buffer* fb, int ind, const geometry_msgs::TransformStamped::ConstPtr& msg);

// main function
int main(int argc, char** argv)
{
  int num_robs;
  int filt_size;
  int t_buff_size;
  ros::Subscriber plane_sub;
  ros::Subscriber* robs_subs;
  ros::Publisher* robs_pubs;
  ros::Publisher plane_pub;
  std::string plane_topic_name;
  std::string* rob_topic_names;
  double local_dir[3];
  Frame_Buffer* fb;
  std::vector<double> tmp_loader;
  double theta;
  double* rob_p;
  double t;
  double t_last;
  double theta_last = 0;
  ros::Rate loop(LOOP_RATE);

  // initilize ros
  ros::init(argc, argv, "mfrmfp_vicon_listener");
  ros::NodeHandle n;

  // load parameters
  if (!ros::param::get("/num_robots", num_robs))
  {
    ROS_FATAL("Misisng Parameter: number of robots.");
    return 0;
  }

  if (!ros::param::get("/local_dir", tmp_loader))
  {
    ROS_FATAL("Misisng Parameter: local dir.");
    return 0;
  }
  else
  {
    local_dir[0] = tmp_loader[0];
    local_dir[1] = tmp_loader[1];
    local_dir[2] = tmp_loader[2];

	  vect_normalize(local_dir);
  }

  fb = new Frame_Buffer(num_robs, local_dir);
  rob_p = new double[num_robs];

  if (!ros::param::get("/plane_vicon_topic", plane_topic_name))
  {
    ROS_FATAL("Missing Parameter: plane topic name.");
    return 0;
  }

  robs_subs = new ros::Subscriber[num_robs];
  robs_pubs = new ros::Publisher[num_robs];
  rob_topic_names = new std::string[num_robs];

  for (int i=0; i < num_robs; i++)
  {
    char param_name[16];
    sprintf(param_name, "/rob%d_vicon_topic", i);
    if (!ros::param::get(param_name, rob_topic_names[i]))
    {
      ROS_FATAL("Missing Parameter: Vicont topic for robot %d.", i);
      return 0;
    }
  }

  // setup publishers and subscribers
  plane_pub = n.advertise<tasc_ros::MFRMFPParentState>("/TASC/Parent/x", 1);

  ROS_INFO("number of robots: %d", num_robs);

  for (int i=0; i < num_robs; i++)
  {
    char topic[32];
    sprintf(topic, "/TASC/Swarm/rob%d/x", i);
    robs_pubs[i] = n.advertise<tasc_ros::MFRMFPNodeState>(topic, 1);
    ROS_INFO("advertising robot %d", i);
  }

  plane_sub = n.subscribe<geometry_msgs::TransformStamped>(plane_topic_name, 1, boost::bind(on_parent_update, fb, _1));

  for (int i=0; i < num_robs; i++)
  {
    robs_subs[i] = n.subscribe<geometry_msgs::TransformStamped>(rob_topic_names[i], 1, boost::bind(on_rob_update, fb, i, _1));
  }

  // spin ros until finished

  while (ros::ok())
  {
    if (fb->pop_latest_frame(&theta, rob_p, &t))
    {
      tasc_ros::MFRMFPParentState msg_p;
      tasc_ros::MFRMFPNodeState* msg_s = new tasc_ros::MFRMFPNodeState[num_robs];

      msg_p.theta = theta;
      msg_p.omega = (theta - theta_last)/0.004;
      msg_p.t = t;

      plane_pub.publish(msg_p);

      for (int i = 0; i < num_robs; i++)
      {
        msg_s[i].pos = rob_p[i];
        msg_s[i].t = t;

        robs_pubs[i].publish(msg_s[i]);
      }

      t_last = t;
      theta_last = theta;

      ROS_INFO("Got frame at t=%f", t);

      delete[] msg_s;
    }
    ros::spinOnce();
    loop.sleep();
  }

  delete fb;
  delete[] rob_p;
  delete[] robs_subs;
  delete[] robs_pubs;
  delete[] rob_topic_names;

  return 0;
}

void on_parent_update(Frame_Buffer* fb, const geometry_msgs::TransformStamped::ConstPtr& msg)
{
  double parent_p[3];
  double parent_q[4];
  double t = msg->header.stamp.sec + msg->header.stamp.nsec/1000000000.0;
  int seq = msg->header.seq;

  parent_p[0] = msg->transform.translation.x;
  parent_p[1] = msg->transform.translation.y;
  parent_p[2] = msg->transform.translation.z;

  parent_q[0] = msg->transform.rotation.w;
  parent_q[1] = msg->transform.rotation.x;
  parent_q[2] = msg->transform.rotation.y;
  parent_q[3] = msg->transform.rotation.z;

  fb->add_plane_dat(parent_p, parent_q, seq, t);
}

void on_rob_update(Frame_Buffer* fb, int ind, const geometry_msgs::TransformStamped::ConstPtr& msg)
{
  double rob_pos[3];
  double t = msg->header.stamp.sec + msg->header.stamp.nsec/1000000000.0;
  int seq = msg->header.seq;

  rob_pos[0] = msg->transform.translation.x;
  rob_pos[1] = msg->transform.translation.y;
  rob_pos[2] = msg->transform.translation.z;

  fb->add_rob_dat(ind, rob_pos, seq, t);
}
