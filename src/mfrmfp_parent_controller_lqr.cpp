/*
 * mfrmfp_parent_controller_lqr.cpp
 *
 * This is a ros node that impliments an LQR controller for the parent system
 * in the TASC controller architecture.
 */

// ROS includes
#include <ros/ros.h>
#include <tasc_ros/MFRMFPParentState.h>
#include <tasc_ros/MFRMFPAbstractState.h>
#include <tasc_ros/aux_func.hpp>
#include <std_msgs/Int8.h>

// boost includes
#include <boost/bind.hpp>
#include <boost/function.hpp>

// standard includes
#include <math.h>

// macros
#define GRAV 9.81
#define PI 3.14159265359

// structs
struct callback_params
{
  double k1;
  double k2;
  double ka;
  double min_m;
  double len;
  int reset;
  ros::Publisher abs_des_pub;
  ros::Publisher abs_des_dot_pub;
  ros::Publisher parent_state_des_pub;
  boost::function<double(double)> des_traj;
  boost::function<double(double)> des_traj_d;
};

// function prototypes
void parent_state_callback(callback_params* params, const tasc_ros::MFRMFPParentState::ConstPtr& msg);
void init_cb(callback_params* params, const std_msgs::Int8::ConstPtr& msg);

// main function
int main(int argc, char** argv)
{
  double amp;
  double freq;
  double off;
  double phi;
  callback_params cb_params;
  ros::Subscriber parent_state_sub;
  ros::Subscriber init_sub;

  // initilize ros
  ros::init(argc, argv, "mfrmfp_parent_controller_lqr");
  ros::NodeHandle n;

  // load parameters
  if (!ros::param::get("/parent_k1", cb_params.k1) || !ros::param::get("/parent_k2", cb_params.k2) || !ros::param::get("/parent_ka", cb_params.ka))
  {
    ROS_ERROR("Missing Parameter: LQR gains");
    return 0;
  }

  ROS_INFO("LQR Gains Loaded: k1=%f k2=%f ka=%f", cb_params.k1, cb_params.k2, cb_params.ka);

  if (!ros::param::get("/min_m", cb_params.min_m) || !ros::param::get("/len", cb_params.len))
  {
    ROS_ERROR("Missing Parameter: physical properties");
    return 0;
  }

  ROS_INFO("Physical Properties of swarm and parent loaded: min_m=%f, len=%f", cb_params.min_m, cb_params.len);

  if (!ros::param::get("/track_amp", amp) || !ros::param::get("/track_freq", freq) || !ros::param::get("/track_off", off) || !ros::param::get("/track_phi", phi))
  {
    ROS_ERROR("Missing Parameter: tracking trajectory");
    return 0;
  }

  ROS_INFO("Tracking trajectory parameters loaded: amp=%f freq=%f off=%f", amp, freq, off);
  cb_params.des_traj = boost::bind(sineusoid, amp, freq, phi, off, _1);
  cb_params.des_traj_d = boost::bind(sineusoid, amp*2.0*PI*freq, freq, phi + PI/2.0, 0.0, _1);
  cb_params.reset = 0;

  // setup publishers
  cb_params.abs_des_pub = n.advertise<tasc_ros::MFRMFPAbstractState>("/TASC/Swarm/abs_state_des", 1);
  cb_params.abs_des_dot_pub = n.advertise<tasc_ros::MFRMFPAbstractState>("/TASC/Swarm/abs_state_des_dot", 1);
  cb_params.parent_state_des_pub = n.advertise<tasc_ros::MFRMFPParentState>("/TASC/Parent/x_d", 1);

  // setup subscribers
  parent_state_sub = n.subscribe<tasc_ros::MFRMFPParentState>("/TASC/Parent/x", 1, boost::bind(parent_state_callback, &cb_params, _1));
  init_sub = n.subscribe<std_msgs::Int8>("/TASC/Sys/init", 1, boost::bind(init_cb, &cb_params, _1));

  // spin ros
  ros::spin();

  return 0;
}

// function definitions
void parent_state_callback(callback_params* params, const tasc_ros::MFRMFPParentState::ConstPtr& msg)
{
  static double error = 0.0;
  static double tau_last = 0.0;
  static double t_last = 0.0;

  tasc_ros::MFRMFPAbstractState a_d;
  tasc_ros::MFRMFPAbstractState a_d_dot;
  tasc_ros::MFRMFPParentState x_p_d;

  if (params->reset)
  {
    error = 0.0;
    tau_last = 0.0;
    t_last = 0.0;
    params->reset = 0;
  }

  x_p_d.theta = params->des_traj(msg->t);
  x_p_d.omega = params->des_traj_d(msg->t);
  x_p_d.t = msg->t;

  a_d.tau = (params->k1*msg->theta + params->k2*msg->omega + params->ka*error)/(GRAV*cos(msg->theta));
  a_d.J = params->min_m*params->len*params->len/4;
  a_d.t = msg->t;

  //a_d_dot.tau = (a_d.tau - tau_last)/(a_d.t - t_last);
  a_d_dot.tau = (a_d.tau - tau_last)/(0.004);
  a_d_dot.J = 0.0;
  a_d_dot.t = a_d.t;

  params->abs_des_pub.publish(a_d);
  params->abs_des_dot_pub.publish(a_d_dot);
  params->parent_state_des_pub.publish(x_p_d);

  error += (params->des_traj(msg->t) - msg->theta)*(msg->t - t_last);
  tau_last = a_d.tau;
  t_last = a_d.t;
}

void init_cb(callback_params* params, const std_msgs::Int8::ConstPtr& msg)
{
  params->reset = 1;
  ROS_INFO("Resetting (count = %d)", msg->data);
}
