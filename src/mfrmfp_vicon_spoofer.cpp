#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <std_msgs/Float32.h>
#include <tasc_ros/quat_func.hpp>
#include <cmath>
#include <vector>
#include <string>

#define PI 3.14159265

void gen_tfs(double theta, double* rob_pos, double* plane_pos, double* plane_rot, double* rob_off, geometry_msgs::TransformStamped* plane_tf, geometry_msgs::TransformStamped* rob_tf, int num_robs);

int main(int argc, char** argv)
{
  // setup ros
  ros::init(argc, argv, "vicon_spoofer");
  ros::NodeHandle n;

  int num_robs;
  double* frequencies;
  double* amplitudes;
  double* phases;
  double* offsets;
  double* rob_off;
  double plane_pos[3];
  double plane_rot[4];
  double theta;
  double* rob_pos;
  std::string plane_topic_name;
  std::string* rob_topic_names;
  std::vector<double> tmp_loader;
  ros::Publisher plane_pub;
  ros::Publisher* rob_pubs;
  ros::Publisher theta_pub;
  ros::Publisher* pos_pubs;
  geometry_msgs::TransformStamped plane_tf;
  geometry_msgs::TransformStamped* rob_tf;
  tf::TransformBroadcaster br;
  tf::Transform transform;
  ros::Rate loop_rate(100);

  // load parameters
  if (!ros::param::get("/num_robots", num_robs))
  {
    ROS_ERROR("Misisng Parameter: number of robots.");
    return 0;
  }

  frequencies = new double[num_robs+1];
  amplitudes = new double[num_robs+1];
  phases = new double[num_robs+1];
  offsets = new double[num_robs+1];
  rob_off = new double[num_robs];
  rob_topic_names = new std::string[num_robs];
  rob_pubs = new ros::Publisher[num_robs];
  pos_pubs = new ros::Publisher[num_robs];

  if (!ros::param::get("/Plane_f", frequencies[0]) || !ros::param::get("/Plane_a", amplitudes[0]) || !ros::param::get("/Plane_p", phases[0]) || !ros::param::get("/Plane_o", offsets[0]))
  {
    ROS_ERROR("Missing Parameter: Plane Wave specs");
    return 0;
  }

  for (int i = 0; i < num_robs; i++)
  {
    char param_name_f[16];
    char param_name_a[16];
    char param_name_p[16];
    char param_name_o[16];
    char param_name_r[16];
    char param_name_m[16];

    sprintf(param_name_f, "/Rob%d_f", i);
    sprintf(param_name_a, "/Rob%d_a", i);
    sprintf(param_name_p, "/Rob%d_p", i);
    sprintf(param_name_o, "/Rob%d_o", i);
    sprintf(param_name_r, "/Rob%d_off", i);
    sprintf(param_name_m, "/rob%d_vicon_topic", i);

    if (!ros::param::get(param_name_f, frequencies[i+1]) || !ros::param::get(param_name_a, amplitudes[i+1]) || !ros::param::get(param_name_p, phases[i+1]) || !ros::param::get(param_name_o, offsets[i+1]) || !ros::param::get(param_name_r, rob_off[i]) || !ros::param::get(param_name_m, rob_topic_names[i]))
    {
      ROS_ERROR("Missing Parameter: Rob %d wave specs", i);
    }
  }

  if (!ros::param::get("/Plane_pos", tmp_loader))
  {
    ROS_ERROR("Missing Plane Position Parameter");
    return 0;
  }
  else
  {
    plane_pos[0] = tmp_loader[0];
    plane_pos[1] = tmp_loader[1];
    plane_pos[2] = tmp_loader[2];
  }

  if (!ros::param::get("/Plane_rot", tmp_loader))
  {
    ROS_ERROR("Missing Plane Rotation Parameter");
    return 0;
  }
  else
  {
    plane_rot[0] = tmp_loader[0];
    plane_rot[1] = tmp_loader[1];
    plane_rot[2] = tmp_loader[2];
    plane_rot[3] = tmp_loader[3];
  }

  if (!ros::param::get("/plane_vicon_topic", plane_topic_name))
  {
    ROS_ERROR("Missing Parameter: plane topic name.");
    return 0;
  }

  ROS_INFO("Parameters loaded, setting up topics...");

  plane_pub = n.advertise<geometry_msgs::TransformStamped>(plane_topic_name, 1);

  theta_pub = n.advertise<std_msgs::Float32>("/spoofer/theta", 1);

  for (int i = 0; i < num_robs; i++)
  {
    char pos_topic_name[16];

    sprintf(pos_topic_name, "/spoofer/pos%d", i);

    rob_pubs[i] = n.advertise<geometry_msgs::TransformStamped>(rob_topic_names[i], 1);
    pos_pubs[i] = n.advertise<std_msgs::Float32>(pos_topic_name, 1);
  }

  ROS_INFO("Topics set up.  Starting....");

  rob_tf = new geometry_msgs::TransformStamped[num_robs];
  rob_pos = new double[num_robs];
  long seq = 0;

  while(ros::ok())
  {
    std_msgs::Float32 theta_msg;
    ros::Time t = ros::Time::now();

    theta = offsets[0] + amplitudes[0]*sin(frequencies[0]*2*PI*ros::Time::now().toSec() + phases[0]);

    for (int i = 0; i < num_robs; i++)
    {
      rob_pos[i] = offsets[i+1] + amplitudes[i+1]*sin(frequencies[i+1]*2*PI*ros::Time::now().toSec() + phases[i+1]);
    }

    gen_tfs(theta, rob_pos, plane_pos, plane_rot, rob_off, &plane_tf, rob_tf, num_robs);

    theta_msg.data = theta;
    plane_tf.header.seq = seq;
    plane_tf.header.stamp = t;
    plane_tf.header.frame_id = "\\world";
    plane_tf.child_frame_id = "\\spoofer\\Plane";

    transform.setOrigin(tf::Vector3(plane_tf.transform.translation.x, plane_tf.transform.translation.y, plane_tf.transform.translation.z));
    transform.setRotation(tf::Quaternion(plane_tf.transform.rotation.x, plane_tf.transform.rotation.y, plane_tf.transform.rotation.z, plane_tf.transform.rotation.w));

    plane_pub.publish(plane_tf);
    theta_pub.publish(theta_msg);
    br.sendTransform(tf::StampedTransform(transform, t, "\\world", "\\spoofer\\Plane"));

    for (int i = 0; i < num_robs; i++)
    {
      char tf_name[16];
      std_msgs::Float32 pos_msg;

      sprintf(tf_name, "\\spoofer\\Rob%d", i);

      pos_msg.data=rob_pos[i];
      rob_tf[i].header.seq = seq;
      rob_tf[i].header.stamp = t;
      rob_tf[i].header.frame_id = "\\world";
      rob_tf[i].child_frame_id = tf_name;

      transform.setOrigin(tf::Vector3(rob_tf[i].transform.translation.x, rob_tf[i].transform.translation.y, rob_tf[i].transform.translation.z));
      transform.setRotation(tf::Quaternion(rob_tf[i].transform.rotation.x, rob_tf[i].transform.rotation.y, rob_tf[i].transform.rotation.z, rob_tf[i].transform.rotation.w));

      rob_pubs[i].publish(rob_tf[i]);
      pos_pubs[i].publish(pos_msg);
      br.sendTransform(tf::StampedTransform(transform, t, "\\world", tf_name));
    }

    seq++;
    ros::spinOnce();
    loop_rate.sleep();
  }

  delete[] frequencies;
  delete[] amplitudes;
  delete[] phases;
  delete[] offsets;
  delete[] rob_tf;
  delete[] rob_topic_names;
  delete[] rob_pubs;
  delete[] pos_pubs;
}

void gen_tfs(double theta, double* rob_pos, double* plane_pos, double* plane_rot, double* rob_off, geometry_msgs::TransformStamped* plane_tf, geometry_msgs::TransformStamped* rob_tf, int num_robs)
{
  double plane_rot_theta[4];
  double plane_rot_total[4];
  double axis_locl[] = {0.0, 1.0, 0.0};
  double dir_locl[] = {1,0, 0.0, 0.0};
  double axis_glob[3];
  double dir_glob[3];

  quat_rot(plane_rot, axis_locl, axis_glob);
  vect_normalize(axis_glob);

  plane_rot_theta[0] = cos(theta/2);
  plane_rot_theta[1] = axis_glob[0]*sin(theta/2);
  plane_rot_theta[2] = axis_glob[1]*sin(theta/2);
  plane_rot_theta[3] = axis_glob[2]*sin(theta/2);

  quat_mult(plane_rot_theta, plane_rot, plane_rot_total);

  plane_tf->transform.translation.x = plane_pos[0];
  plane_tf->transform.translation.y = plane_pos[1];
  plane_tf->transform.translation.z = plane_pos[2];

  plane_tf->transform.rotation.w = plane_rot_total[0];
  plane_tf->transform.rotation.x = plane_rot_total[1];
  plane_tf->transform.rotation.y = plane_rot_total[2];
  plane_tf->transform.rotation.z = plane_rot_total[3];

  quat_rot(plane_rot_total, dir_locl, dir_glob);
  vect_normalize(dir_glob);

  for (int i = 0; i < num_robs; i++)
  {
    double rob_pos_plane_x[3];
    double rob_pos_plane_y[3];
    double rob_pos_plane[3];
    double rob_pos_total[3];

    vect_scalar_mult(dir_glob, rob_pos[i], rob_pos_plane_x);
    vect_scalar_mult(axis_glob, rob_off[i], rob_pos_plane_y);
    vect_add(rob_pos_plane_x, rob_pos_plane_y, rob_pos_plane);
    vect_add(plane_pos, rob_pos_plane, rob_pos_total);

    rob_tf[i].transform.translation.x = rob_pos_total[0];
    rob_tf[i].transform.translation.y = rob_pos_total[1];
    rob_tf[i].transform.translation.z = rob_pos_total[2];

    rob_tf[i].transform.rotation.w = plane_rot_total[0];
    rob_tf[i].transform.rotation.x = plane_rot_total[1];
    rob_tf[i].transform.rotation.y = plane_rot_total[2];
    rob_tf[i].transform.rotation.z = plane_rot_total[3];
  }
}
