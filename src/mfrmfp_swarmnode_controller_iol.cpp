/*
 * mfrmfp_swarmnode_controller_iol.cpp
 *
 * This is a ros node that represents the controller for a single node of the
 * swarm.  It takes it's current position, the desired and current abstract
 * states, and the global swarm observations and calculates an output based on
 * these values.
 */

// ROS includes
#include <ros/ros.h>
#include <tasc_ros/MFRMFPGlobObs.h>
#include <tasc_ros/MFRMFPNodeState.h>
#include <tasc_ros/MFRMFPNodeInput.h>
#include <tasc_ros/MFRMFPAbstractState.h>
#include <tasc_ros/aux_func.hpp>

// boost includes
#include <boost/bind.hpp>

// standard includes
#include <stdio.h>
#include <math.h>

// function prototypes
void abs_state_callback(double* abs_state, char* update_flag, double* t, const tasc_ros::MFRMFPAbstractState::ConstPtr& msg);
void global_obs_callback(double* glob_obs, char* update_flag, double* t, const tasc_ros::MFRMFPGlobObs::ConstPtr& msg);
void node_state_callback(double* node_state, char* update_flag, double* t, const tasc_ros::MFRMFPNodeState::ConstPtr& msg);

// main function
int main(int argc, char** argv)
{
  int ind = -1;
  char node_name[32];
  char topic_name[32];
  char mass_param_name[8];
  int num_robs;
  double k1;
  double k2;
  double m;
  double v_max;
  double t = 0.0;
  double abs_state[2];
  double abs_state_des[2];
  double abs_state_des_dot[2];
  double glob_obs[4];
  double node_state;
  char update_flags[5];
  ros::Subscriber abs_state_sub;
  ros::Subscriber abs_state_des_sub;
  ros::Subscriber abs_state_des_dot_sub;
  ros::Subscriber glob_obs_sub;
  ros::Subscriber node_state_sub;
  ros::Publisher node_input_pub;

  // initilize ROS
  sprintf(node_name, "swarm_controller_rob%d", ind);
  ros::init(argc, argv, node_name);
  ros::NodeHandle n;

  // load parameters
  if (argc < 2)
  {
    ROS_ERROR("index for this node not specified");
    return 0;
  }

  ind = atoi(argv[1]);

  if (!ros::param::get("/num_robots", num_robs))
  {
    ROS_ERROR("Missing Parameter: number of robots");
    return 0;
  }

  if (!ros::param::get("/swarm_k1", k1) || !ros::param::get("/swarm_k2", k2))
  {
    ROS_ERROR("Missing Parameter: swarm controller gains");
    return 0;
  }

  if (ind < 0 || ind >= num_robs)
  {
    ROS_ERROR("invalid index %d", ind);
    return 0;
  }

  sprintf(mass_param_name, "/mass%d", ind);
  if (!ros::param::get(mass_param_name, m))
  {
    ROS_ERROR("Missing Parameter: mass for node %d)", ind);
    return 0;
  }

  if (!ros::param::get("/rob_v_max", v_max))
  {
    ROS_ERROR("Missing Parameter: max robot velocity");
    return 0;
  }

  ROS_INFO("Loaded Parameters, running controller for robot %d of %d using gains k1=%f and k2=%f with max velocity %f", ind, num_robs, k1, k2, v_max);

  // set up subscribers
  sprintf(topic_name, "/TASC/Swarm/rob%d/x", ind);
  abs_state_sub = n.subscribe<tasc_ros::MFRMFPAbstractState>("/TASC/Swarm/abs_state", 1, boost::bind(abs_state_callback, abs_state, update_flags, &t, _1));
  abs_state_des_sub = n.subscribe<tasc_ros::MFRMFPAbstractState>("/TASC/Swarm/abs_state_des", 1, boost::bind(abs_state_callback, abs_state_des, update_flags+1, &t, _1));
  abs_state_des_dot_sub = n.subscribe<tasc_ros::MFRMFPAbstractState>("/TASC/Swarm/abs_state_des_dot", 1, boost::bind(abs_state_callback, abs_state_des_dot, update_flags+2, &t, _1));
  glob_obs_sub = n.subscribe<tasc_ros::MFRMFPGlobObs>("/TASC/Swarm/global_obs", 1, boost::bind(global_obs_callback, glob_obs, update_flags+3, &t, _1));
  node_state_sub = n.subscribe<tasc_ros::MFRMFPNodeState>(topic_name, 1, boost::bind(node_state_callback, &node_state, update_flags+4, &t, _1));

  // set up publishers
  sprintf(topic_name, "/TASC/Swarm/rob%d/u", ind);
  node_input_pub = n.advertise<tasc_ros::MFRMFPNodeInput>(topic_name, 1);

  // main loop
  while (ros::ok())
  {
    bool update_ready = true;

    // check if ready to update control input;
    for (int i = 0; i < 5; i++)
    {
      if (!update_flags[i])
      {
        update_ready = false;
        break;
      }
    }

    // if ready, update controller
    if (update_ready)
    {
      double del_abs[2];
      double iol_gains[2];
      tasc_ros::MFRMFPNodeInput msg;

      // calculate terms of the controller
      del_abs[0] = k1*(abs_state_des[0] - abs_state[0]) + abs_state_des_dot[0];
      del_abs[1] = k2*(abs_state_des[1] - abs_state[1]) + abs_state_des_dot[1];

      iol_gains[0] = (m*glob_obs[2] - m*node_state*glob_obs[1])/glob_obs[3];
      iol_gains[1] = 0.5*(m*node_state*glob_obs[0] - m*glob_obs[1])/glob_obs[3];

      // send message to simulation/robot
      msg.vel = iol_gains[0]*del_abs[0] + iol_gains[1]*del_abs[1];
      msg.t = t/5.0;

      if (abs(msg.vel) > v_max)
      {
        msg.vel = sgn(msg.vel)*v_max;
      }

      node_input_pub.publish(msg);

      // prepare to recieve next batch.
      update_flags[0] = 0;
      update_flags[1] = 0;
      update_flags[2] = 0;
      update_flags[3] = 0;
      update_flags[4] = 0;

      t = 0.0;
    }

    // spin ros
    ros::spinOnce();
  }

  return 0;
}

// function definitions
void abs_state_callback(double* abs_state, char* update_flag, double* t, const tasc_ros::MFRMFPAbstractState::ConstPtr& msg)
{
  if (!*update_flag)
  {
    abs_state[0] = msg->tau;
    abs_state[1] = msg->J;

    *t += msg->t;
    *update_flag = 1;
  }
}

void global_obs_callback(double* glob_obs, char* update_flag, double* t, const tasc_ros::MFRMFPGlobObs::ConstPtr& msg)
{
  if (!*update_flag)
  {
    glob_obs[0] = msg->sum0;
    glob_obs[1] = msg->sum1;
    glob_obs[2] = msg->sum2;
    glob_obs[3] = msg->sum3;

    *t += msg->t;
    *update_flag = 1;
  }
}

void node_state_callback(double* node_state, char* update_flag, double* t, const tasc_ros::MFRMFPNodeState::ConstPtr& msg)
{
  if (!*update_flag)
  {
    *node_state = msg->pos;

    *t += msg->t;
    *update_flag = 1;
  }
}
