/*
 * data_buffer.hpp
 *
 * Contains classes for creating data buffer and syncing for
 * incomming vicon data
 */

#include <ros/ros.h>
#include <tasc_ros/quat_func.hpp>
//#include "quat_func.hpp"

#include <queue>
#include <cstring>
//#include <iostream>

#ifndef __TASC_ROS_DATA_BUFFER__
#define __TASC_ROS_DATA_BUFFER__

class Frame
{
private:
  double _plane_pos[3];
  double _plane_rot[4];
  double* _rob_pos;
  double _local_dir[3];
  bool* _data_entered;
  int _num_robs;
  int _seq;
  double _t;
  Frame* _prev_frame = NULL;

public:
  Frame(int num_robs, double* local_dir);
  Frame(int num_robs, double* local_dir, double* plane_pos, double* plane_rot, double* rob_pos);
  ~Frame();

  void set_plane_dat(double* plane_pos, double* plane_rot);
  void set_rob_pos(int rob_ind, double* rob_pos);
  void set_metadata(int seq, double t);
  void set_prev_frame(Frame* prev);

  void get_plane_dat(double* plane_pos, double* plane_rot);
  void get_rob_pos(int rob_ind, double* rob_pos);
  Frame* get_prev_frame();
  int get_seq();
  int get_time();

  double get_theta();
  double get_rob_p(int rob_ind);

  bool get_data_entered();
};

class Frame_Buffer
{
private:
  Frame* _head;
  double _local_dir[3];
  int _num_robs;

public:
  Frame_Buffer(int num_robs, double* local_dir);
  ~Frame_Buffer();

  void add_plane_dat(double* plane_pos, double* plane_rot, int seq, double time);
  void add_rob_dat(int rob_ind, double* rob_pos, int seq, double time);

  bool pop_latest_frame(double* theta, double* rob_pos, double* time);
};

Frame::Frame(int num_robs, double* local_dir)
{
  _num_robs = num_robs;
  _rob_pos = new double[_num_robs*3];
  _data_entered = new bool[_num_robs+1];

  for (int i=0; i<_num_robs+1; i++)
  {
    _data_entered[i] = false;
  }

  memcpy(_local_dir, local_dir, sizeof(double)*3);
}

Frame::Frame(int num_robs, double* local_dir, double* plane_pos, double* plane_rot, double* rob_pos)
{
  _num_robs = num_robs;
  _rob_pos = new double[_num_robs*3];
  _data_entered = new bool[_num_robs+1];

  for (int i=0; i<_num_robs+1; i++)
  {
    _data_entered[i] = true;
  }

  memcpy(_plane_pos, plane_pos, sizeof(double)*3);
  memcpy(_plane_rot, plane_rot, sizeof(double)*4);
  memcpy(_rob_pos, rob_pos, sizeof(double)*3*_num_robs);
  memcpy(_local_dir, local_dir, sizeof(double)*3);
}

Frame::~Frame()
{
  delete [] _rob_pos;
  delete [] _data_entered;
}

void Frame::set_plane_dat(double* plane_pos, double* plane_rot)
{
  memcpy(_plane_pos, plane_pos, sizeof(double)*3);
  memcpy(_plane_rot, plane_rot, sizeof(double)*4);
  _data_entered[0] = true;
}

void Frame::set_rob_pos(int rob_ind, double* rob_pos)
{
  memcpy(_rob_pos+(rob_ind*3), rob_pos, sizeof(double)*3);
  _data_entered[rob_ind+1] = true;
}

void Frame::set_metadata(int seq, double t)
{
  _seq = seq;
  _t = t;
}

void Frame::set_prev_frame(Frame* prev)
{
  _prev_frame = prev;
}

void Frame::get_plane_dat(double* plane_pos, double* plane_rot)
{
  memcpy(plane_pos, _plane_pos, sizeof(double)*3);
  memcpy(plane_rot, _plane_rot, sizeof(double)*4);
}

void Frame::get_rob_pos(int rob_ind, double* rob_pos)
{
  memcpy(rob_pos, _rob_pos+(rob_ind*3), sizeof(double)*3);
}

Frame* Frame::get_prev_frame()
{
  return _prev_frame;
}

int Frame::get_seq()
{
  return _seq;
}

int Frame::get_time()
{
  return _t;
}

double Frame::get_theta()
{
  double dir[3];

  quat_rot(_plane_rot, _local_dir, dir);
  vect_normalize(dir);

  return -asin(dir[2]);
}

double Frame::get_rob_p(int rob_ind)
{
  double neg_p[3];
  double diff[3];
  double dir[3];

  quat_rot(_plane_rot, _local_dir, dir);
  vect_normalize(dir);
  vect_scalar_mult(_plane_pos, -1.0, neg_p);
  vect_add(neg_p, _rob_pos+rob_ind*3, diff);

  return vect_dot(diff, dir);
}

bool Frame::get_data_entered()
{
  for (int i=0; i<_num_robs+1; i++)
  {
    if (!_data_entered[i])
    {
      return false;
    }
  }
  return true;
}

Frame_Buffer::Frame_Buffer(int num_robs, double* local_dir)
{
  _head = NULL;
  _num_robs = num_robs;
  memcpy(_local_dir, local_dir, sizeof(double)*3);
}

Frame_Buffer::~Frame_Buffer()
{
  Frame* prev = _head;

  while (prev != NULL)
  {
    Frame* cur = prev;
    prev = cur->get_prev_frame();

    delete cur;
  }
}

void Frame_Buffer::add_plane_dat(double* plane_pos, double* plane_rot, int seq, double time)
{
  Frame* cur_frame = _head;

  if (_head == NULL)
  {
    _head = new Frame(_num_robs, _local_dir);
    _head->set_plane_dat(plane_pos, plane_rot);
    _head->set_metadata(seq, time);
    return;
  }
  else if (_head->get_seq() < seq)
  {
    Frame* new_frame = new Frame(_num_robs, _local_dir);
    new_frame->set_plane_dat(plane_pos, plane_rot);
    new_frame->set_metadata(seq, time);
    new_frame->set_prev_frame(_head);
    _head = new_frame;
    return;
  }

  while (cur_frame != NULL)
  {
    if (cur_frame->get_seq() == seq)
    {
      cur_frame->set_plane_dat(plane_pos, plane_rot);
      return;
    }
    else if (cur_frame->get_prev_frame() != NULL)
    {
      if (cur_frame->get_seq() > seq && cur_frame->get_prev_frame()->get_seq() < seq)
      {
        Frame* new_frame = new Frame(_num_robs, _local_dir);
        new_frame->set_plane_dat(plane_pos, plane_rot);
        new_frame->set_metadata(seq, time);
        new_frame->set_prev_frame(cur_frame->get_prev_frame());
        cur_frame->set_prev_frame(new_frame);
        return;
      }
    }
    else if (cur_frame->get_seq() > seq)
    {
      Frame* new_frame = new Frame(_num_robs, _local_dir);
      new_frame->set_plane_dat(plane_pos, plane_rot);
      new_frame->set_metadata(seq, time);
      cur_frame->set_prev_frame(new_frame);
      return;
    }
    cur_frame = cur_frame->get_prev_frame();
  }

  ROS_ERROR("Bad Frame tried to enter plane data, seq: %d t: %f", seq, time);
  //std::cout << "Bad Frame tried to enter plane data. seq: " << seq << " t: " << time << std::endl;
}

void Frame_Buffer::add_rob_dat(int rob_ind, double* rob_pos, int seq, double time)
{
  Frame* cur_frame = _head;

  if (_head == NULL)
  {
    _head = new Frame(_num_robs, _local_dir);
    _head->set_rob_pos(rob_ind, rob_pos);
    _head->set_metadata(seq, time);
    return;
  }
  else if (_head->get_seq() < seq)
  {
    Frame* new_frame = new Frame(_num_robs, _local_dir);
    new_frame->set_rob_pos(rob_ind, rob_pos);
    new_frame->set_metadata(seq, time);
    new_frame->set_prev_frame(_head);
    _head = new_frame;
    return;
  }

  while (cur_frame != NULL)
  {
    if (cur_frame->get_seq() == seq)
    {
      cur_frame->set_rob_pos(rob_ind, rob_pos);
      return;
    }
    else if (cur_frame->get_prev_frame() != NULL)
    {
      if (cur_frame->get_seq() > seq && cur_frame->get_prev_frame()->get_seq() < seq)
      {
        Frame* new_frame = new Frame(_num_robs, _local_dir);
        new_frame->set_rob_pos(rob_ind, rob_pos);
        new_frame->set_metadata(seq, time);
        new_frame->set_prev_frame(cur_frame->get_prev_frame());
        cur_frame->set_prev_frame(new_frame);
        return;
      }
    }
    else if (cur_frame->get_seq() > seq)
    {
      Frame* new_frame = new Frame(_num_robs, _local_dir);
      new_frame->set_rob_pos(rob_ind, rob_pos);
      new_frame->set_metadata(seq, time);
      cur_frame->set_prev_frame(new_frame);
      return;
    }
    cur_frame = cur_frame->get_prev_frame();
  }

  ROS_ERROR("Bad Frame tried to enter rob %d data, seq: %d t: %f", rob_ind, seq, time);
  //std::cout << "Bad Frame tried to enter rob " << rob_ind << " data. seq: " << seq << " t: " << time << std::endl;
}

bool Frame_Buffer::pop_latest_frame(double* theta, double* rob_p, double* time)
{
  Frame* cur_frame = _head;
  Frame* next_frame = NULL;

  while(1)
  {
    if (cur_frame == NULL)
    {
      return false;
    }
    else if(cur_frame->get_data_entered())
    {
      *theta = cur_frame->get_theta();
      for (int i=0; i<_num_robs; i++)
      {
        rob_p[i] = cur_frame->get_rob_p(i);
      }
      *time = cur_frame->get_time();

      if (next_frame != NULL)
      {
        next_frame->set_prev_frame(NULL);
      }
      else
      {
        _head = NULL;
      }
      while(cur_frame != NULL)
      {
        Frame* to_del = cur_frame;
        cur_frame = cur_frame->get_prev_frame();
        delete to_del;
      }
      return true;
    }
    next_frame = cur_frame;
    cur_frame = cur_frame->get_prev_frame();
  }
}

#endif
