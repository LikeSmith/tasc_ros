#include "../data_buffer.hpp"
#include "../quat_func.hpp"
#include <iostream>

using namespace std;

void gen_tfs(double theta, double* rob_p, double* plane_off, double* plane_rot_const, double* rob_off, double* plane_pos, double* plane_rot, double* rob_pos, int num_robs)
{
  double plane_rot_theta[4];
  double axis_locl[] = {0.0, 1.0, 0.0};
  double dir_locl[] = {1,0, 0.0, 0.0};
  double axis_glob[3];
  double dir_glob[3];

  quat_rot(plane_rot_const, axis_locl, axis_glob);
  vect_normalize(axis_glob);

  plane_rot_theta[0] = cos(theta/2);
  plane_rot_theta[1] = axis_glob[0]*sin(theta/2);
  plane_rot_theta[2] = axis_glob[1]*sin(theta/2);
  plane_rot_theta[3] = axis_glob[2]*sin(theta/2);

  quat_mult(plane_rot_theta, plane_rot_const, plane_rot);

  plane_pos[0] = plane_off[0];
  plane_pos[1] = plane_off[1];
  plane_pos[2] = plane_off[2];

  quat_rot(plane_rot, dir_locl, dir_glob);
  vect_normalize(dir_glob);

  for (int i = 0; i < num_robs; i++)
  {
    double rob_pos_plane_x[3];
    double rob_pos_plane_y[3];
    double rob_pos_plane[3];
    double rob_pos_total[3];

    vect_scalar_mult(dir_glob, rob_p[i], rob_pos_plane_x);
    vect_scalar_mult(axis_glob, rob_off[i], rob_pos_plane_y);
    vect_add(rob_pos_plane_x, rob_pos_plane_y, rob_pos_plane);
    vect_add(plane_pos, rob_pos_plane, rob_pos_total);

    rob_pos[i*3+0] = rob_pos_total[0];
    rob_pos[i*3+1] = rob_pos_total[1];
    rob_pos[i*3+2] = rob_pos_total[2];
  }
}

int main()
{
  double theta = 0.0;
  double rob_p[] = {-0.5, 0.5, -0.25, 0.25};
  double plane_off[] = {0.0, 0.0, 0.5};
  double plane_rot_const[] = {1.0, 0.0, 0.0, 0.0};
  double rob_off[] = {-0.375, -0.125, 0.125, 0.375};
  double plane_pos[3];
  double plane_rot[4];
  double rob_pos[12];
  int num_robs = 4;
  double local_dir[] = {1.0, 0.0, 0.0};
  double plane_pos_cpy[3];
  double plane_rot_cpy[4];

  Frame f(num_robs, local_dir);

  gen_tfs(theta, rob_p, plane_off, plane_rot_const, rob_off, plane_pos, plane_rot, rob_pos, num_robs);

  cout << "is data entered? " << f.get_data_entered() << endl;

  f.set_plane_dat(plane_pos, plane_rot);
  for (int i=0; i<num_robs; i++)
  {
    f.set_rob_pos(i, rob_pos+3*i);
  }
  f.set_metadata(1, 0.0);

  cout << "is data entered? " << f.get_data_entered() << endl;

  cout << "Calculated Data" << endl;
  cout << "-------------------" << endl;
  cout << "Plane Data:" << endl;
  cout << "  Theta: " << theta << endl;
  cout << "  Pos: [" << plane_pos[0] << ',' << plane_pos[1] << ',' << plane_pos[2] << ']' << endl;
  cout << "  Rot: [" << plane_rot[0] << ',' << plane_rot[1] << ',' << plane_rot[2] << ',' << plane_rot[3] << ']' << endl;
  cout << "Rob Data:" << endl;
  for (int i=0; i<num_robs; i++)
  {
    cout << "  Rob " << i << ':' << endl;
    cout << "    p: " << rob_p[i] << endl;
    cout << "    pos: [" << rob_pos[i*3+0] << ',' << rob_pos[i*3+1] << ',' << rob_pos[i*3+2] << ']' << endl;
  }

  f.get_plane_dat(plane_pos_cpy, plane_rot_cpy);

  cout << endl;
  cout << "Stored Data" << endl;
  cout << "-------------------" << endl;
  cout << "seq: " << f.get_seq() << endl;
  cout << "t: " << f.get_time() << endl;
  cout << "Plane Data:" << endl;
  cout << "  Theta: " << f.get_theta() << endl;
  cout << "  Pos: [" << plane_pos_cpy[0] << ',' << plane_pos_cpy[1] << ',' << plane_pos_cpy[2] << ']' << endl;
  cout << "  Rot: [" << plane_rot_cpy[0] << ',' << plane_rot_cpy[1] << ',' << plane_rot_cpy[2] << ',' << plane_rot_cpy[3] << ']' << endl;
  cout << "Rob Data:" << endl;
  for (int i=0; i<num_robs; i++)
  {
    double pos[3];
    f.get_rob_pos(i, pos);
    cout << "  Rob " << i << ':' << endl;
    cout << "    p: " << f.get_rob_p(i) << endl;
    cout << "    pos: [" << pos[0] << ',' << pos[1] << ',' << pos[2] << ']' << endl;
  }

  return 0;
}
