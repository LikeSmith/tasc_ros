#include "../data_buffer.hpp"
#include "../quat_func.hpp"
#include <iostream>

using namespace std;

void gen_tfs(double theta, double* rob_p, double* plane_off, double* plane_rot_const, double* rob_off, double* plane_pos, double* plane_rot, double* rob_pos, int num_robs)
{
  double plane_rot_theta[4];
  double axis_locl[] = {0.0, 1.0, 0.0};
  double dir_locl[] = {1,0, 0.0, 0.0};
  double axis_glob[3];
  double dir_glob[3];

  quat_rot(plane_rot_const, axis_locl, axis_glob);
  vect_normalize(axis_glob);

  plane_rot_theta[0] = cos(theta/2);
  plane_rot_theta[1] = axis_glob[0]*sin(theta/2);
  plane_rot_theta[2] = axis_glob[1]*sin(theta/2);
  plane_rot_theta[3] = axis_glob[2]*sin(theta/2);

  quat_mult(plane_rot_theta, plane_rot_const, plane_rot);

  plane_pos[0] = plane_off[0];
  plane_pos[1] = plane_off[1];
  plane_pos[2] = plane_off[2];

  quat_rot(plane_rot, dir_locl, dir_glob);
  vect_normalize(dir_glob);

  for (int i = 0; i < num_robs; i++)
  {
    double rob_pos_plane_x[3];
    double rob_pos_plane_y[3];
    double rob_pos_plane[3];
    double rob_pos_total[3];

    vect_scalar_mult(dir_glob, rob_p[i], rob_pos_plane_x);
    vect_scalar_mult(axis_glob, rob_off[i], rob_pos_plane_y);
    vect_add(rob_pos_plane_x, rob_pos_plane_y, rob_pos_plane);
    vect_add(plane_pos, rob_pos_plane, rob_pos_total);

    rob_pos[i*3+0] = rob_pos_total[0];
    rob_pos[i*3+1] = rob_pos_total[1];
    rob_pos[i*3+2] = rob_pos_total[2];
  }
}

void print_frame(double theta_p, double theta_c, double*rob_p_p, double* rob_p_c, int time, double seq, int num_robs)
{
  cout << "Frame:" << endl;
  cout << "  seq: " << seq << endl;
  cout << "  time: " << time << endl;
  cout << "  perscribed data" << endl;
  cout << "    Theta: " << theta_p << endl;
  for (int i=0; i<num_robs; i++)
  {
    cout << "    Rob " << i << " p:" << rob_p_p[i] << endl;
  }
  cout << "  copied data" << endl;
  cout << "    Theta: " << theta_c << endl;
  for (int i=0; i<num_robs; i++)
  {
    cout << "    Rob " << i << " p:" << rob_p_c[i] << endl;
  }
}

int main()
{
  double theta1_p = 0.0;
  double theta2_p = 0.1;
  double theta3_p = 0.2;
  double theta4_p = 0.3;
  double rob_p1_p[] = {-0.5, 0.5, -0.15, 0.15};
  double rob_p2_p[] = {-0.4, 0.4, -0.25, 0.25};
  double rob_p3_p[] = {-0.3, 0.3, -0.35, 0.35};
  double rob_p4_p[] = {-0.2, 0.2, -0.45, 0.45};
  double plane_off[] = {0.0, 0.0, 0.5};
  double plane_rot_const[] = {1.0, 0.0, 0.0, 0.0};
  double rob_off[] = {-0.375, -0.125, 0.125, 0.375};
  double local_dir[] = {1.0, 0.0, 0.0};

  double plane_pos1[3];
  double plane_pos2[3];
  double plane_pos3[3];
  double plane_pos4[3];
  double plane_rot1[4];
  double plane_rot2[4];
  double plane_rot3[4];
  double plane_rot4[4];
  double rob_pos1[12];
  double rob_pos2[12];
  double rob_pos3[12];
  double rob_pos4[12];

  double theta;
  double rob_p[4];
  double time;

  Frame_Buffer f_buff(4, local_dir);

  gen_tfs(theta1_p, rob_p1_p, plane_off, plane_rot_const, rob_off, plane_pos1, plane_rot1, rob_pos1, 4);
  gen_tfs(theta2_p, rob_p2_p, plane_off, plane_rot_const, rob_off, plane_pos2, plane_rot2, rob_pos2, 4);
  gen_tfs(theta3_p, rob_p3_p, plane_off, plane_rot_const, rob_off, plane_pos3, plane_rot3, rob_pos3, 4);
  gen_tfs(theta4_p, rob_p4_p, plane_off, plane_rot_const, rob_off, plane_pos4, plane_rot4, rob_pos4, 4);

  if (f_buff.pop_latest_frame(&theta, rob_p, &time))
  {
    cout << "Error 1" << endl;
    return 0;
  }

  f_buff.add_plane_dat(plane_pos1, plane_rot1, 1, 0.001);
  f_buff.add_rob_dat(0, rob_pos1+3*0, 1, 0.001);
  f_buff.add_rob_dat(1, rob_pos1+3*1, 1, 0.001);
  f_buff.add_rob_dat(2, rob_pos1+3*2, 1, 0.001);
  f_buff.add_rob_dat(3, rob_pos1+3*3, 1, 0.001);

  f_buff.add_rob_dat(0, rob_pos3+3*0, 3, 0.003);
  f_buff.add_rob_dat(1, rob_pos3+3*1, 3, 0.003);
  f_buff.add_rob_dat(2, rob_pos3+3*2, 3, 0.003);
  f_buff.add_rob_dat(3, rob_pos3+3*3, 3, 0.003);

  f_buff.add_plane_dat(plane_pos2, plane_rot2, 2, 0.002);

  if (f_buff.pop_latest_frame(&theta, rob_p, &time))
  {
    print_frame(theta1_p, theta, rob_p1_p, rob_p, time, 1, 4);
  }
  else
  {
    cout << "Error 2" << endl;
    return 0;
  }

  f_buff.add_rob_dat(0, rob_pos2+3*0, 2, 0.002);
  f_buff.add_rob_dat(1, rob_pos2+3*1, 2, 0.002);
  f_buff.add_rob_dat(2, rob_pos2+3*2, 2, 0.002);
  f_buff.add_rob_dat(3, rob_pos2+3*3, 2, 0.002);

  if (f_buff.pop_latest_frame(&theta, rob_p, &time))
  {
    print_frame(theta2_p, theta, rob_p2_p, rob_p, time, 2, 4);
  }
  else
  {
    cout << "Error 3" << endl;
    return 0;
  }

  f_buff.add_plane_dat(plane_pos3, plane_rot3, 3, 0.003);

  f_buff.add_plane_dat(plane_pos1, plane_rot4, 4, 0.004);
  f_buff.add_rob_dat(0, rob_pos4+3*0, 4, 0.004);
  f_buff.add_rob_dat(1, rob_pos4+3*1, 4, 0.004);
  f_buff.add_rob_dat(2, rob_pos4+3*2, 4, 0.004);
  f_buff.add_rob_dat(3, rob_pos4+3*3, 4, 0.004);

  if (f_buff.pop_latest_frame(&theta, rob_p, &time))
  {
    print_frame(theta4_p, theta, rob_p4_p, rob_p, time, 4, 4);
  }
  else
  {
    cout << "Error 4" << endl;
    return 0;
  }

  return 0;
}
