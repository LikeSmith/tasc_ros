/*
 * aux_func.hpp
 *
 * This header contains auxilary functions to used in the tasc_ros package.
 */

// Includes
#include <cmath>
#include <cstring>

#ifndef __TASC_ROS_AUX_FUNC__
#define __TASC_ROS_AUX_FUNC__

// macros
#define PI 3.14159265359

// Function prototypes:
double sineusoid(double amp, double freq, double phi, double off, double t);
double sgn(double x);
double sat(double x);
double sgm(double x);
double mean(double* data, int n);
double median(double* data, int n);
void sort_lth(double* data, int n);

// Function definitions
double sineusoid(double amp, double freq, double phi, double off, double t)
{
  return amp*sin(2.0*PI*freq*t + phi) + off;
}

double sgn(double x)
{
  if (x > 0.0)
  {
    return 1.0;
  }
  else if (x < 0.0)
  {
    return -1.0;
  }
  else
  {
    return 0.0;
  }
}

double sat(double x)
{
  if (abs(x) > 1)
  {
    return sgn(x);
  }
  else
  {
    return x;
  }
}

double sgm(double x)
{
  return tanh(x);
}

double mean(double* data, int n)
{
  double res = 0.0;
  for (int i = 0; i < n; i++)
  {
    res += data[i] / (double)n;
  }

  return res;
}

double median(double* data, int n)
{
  double* cpy = new double[n];
  double res;
  memcpy(cpy, data, n*sizeof(double));

  sort_lth(cpy, n);

  if (n%2 == 0)
  {
    res = (cpy[n/2] + cpy[n/2+1])/2.0;
  }
  else
  {
    res = cpy[n/2+1];
  }

  delete[] cpy;
  return res;
}

void sort_lth(double* data, int n)
{
  for (int i = 0; i < n; i++)
  {
    int least_ind = i;
    double tmp;

    for (int j = i+1; j < n; j++)
    {
      if (data[j] < data[least_ind])
      {
        least_ind = j;
      }
    }

    tmp = data[i];
    data[i] = data[least_ind];
    data[least_ind] = tmp;
  }
}

#endif
