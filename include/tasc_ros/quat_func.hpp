/*
 * quat_func.hpp
 *
 * This header contains functions for doing quadratic math.
 */

#ifndef __TASC_ROS_QUAD_FUNC__
#define __TASC_ROS_QUAD_FUNC__

// includes
#import <cmath>

// function prototypes
void quat_mult(double* q1, double* q2, double* res);
void quat_inv(double* q, double* res);
void quat_rot(double* q, double* v, double* res);
void quat_scalar_mult(double* q1, double a, double* res);
double quat_norm(double* q1);
void quat_normalize(double* q1);
void vect_add(double* v1, double*v2, double* res);
void vect_scalar_mult(double* v1, double a, double* res);
void vect_cross(double* v1, double* v2, double* res);
double vect_dot(double* v1, double* v2);
double vect_norm(double* v1);
void vect_normalize(double* v1);

// function definitions
void quat_mult(double* q1, double* q2, double* res)
{
  res[0] = q1[0]*q2[0] - q1[1]*q2[1] - q1[2]*q2[2] - q1[3]*q2[3];
  res[1] = q1[0]*q2[1] + q1[1]*q2[0] + q1[2]*q2[3] - q1[3]*q2[2];
  res[2] = q1[0]*q2[2] - q1[1]*q2[3] + q1[2]*q2[0] + q1[3]*q2[1];
  res[3] = q1[0]*q2[3] + q1[1]*q2[2] - q1[2]*q2[1] + q1[3]*q2[0];
}

void quat_inv(double* q, double* res)
{
  res[0] = q[0];
  res[1] = -q[1];
  res[2] = -q[2];
  res[3] = -q[3];
}

void quat_rot(double* q, double* v, double* res)
{
  double q_inv[4];
  double v_quad[4];
  double tmp[4];

  v_quad[0] = 0.0;
  v_quad[1] = v[0];
  v_quad[2] = v[1];
  v_quad[3] = v[2];

  quat_inv(q, q_inv);
  quat_mult(q, v_quad, tmp);
  quat_mult(tmp, q_inv, v_quad);

  res[0] = v_quad[1];
  res[1] = v_quad[2];
  res[2] = v_quad[3];
}

void quat_scalar_mult(double* q1, double a, double* res)
{
  res[0] = a*q1[0];
  res[1] = a*q1[1];
  res[2] = a*q1[2];
  res[3] = a*q1[3];
}

double quat_norm(double* q1)
{
  return sqrt(q1[0]*q1[0] + q1[1]*q1[1] + q1[2]*q1[2] + q1[3]*q1[3]);
}

void quat_normalize(double* q1)
{
  double n = quat_norm(q1);

  q1[0] = q1[0]/n;
  q1[1] = q1[1]/n;
  q1[2] = q1[2]/n;
  q1[3] = q1[3]/n;
}

void vect_add(double* v1, double*v2, double* res)
{
  res[0] = v1[0] + v2[0];
  res[1] = v1[1] + v2[1];
  res[2] = v1[2] + v2[2];
}

void vect_scalar_mult(double* v1, double a, double* res)
{
  res[0] = a*v1[0];
  res[1] = a*v1[1];
  res[2] = a*v1[2];
}

void vect_cross(double* v1, double* v2, double* res)
{
  res[0] = v1[1]*v2[2] - v1[2]*v2[1];
  res[1] = v1[2]*v2[0] - v1[0]*v2[2];
  res[2] = v1[0]*v2[1] - v1[1]*v2[0];
}

double vect_dot(double* v1, double* v2)
{
  return v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2];
}

double vect_norm(double* v1)
{
  return sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
}

void vect_normalize(double* v1)
{
  double n = vect_norm(v1);

  v1[0] = v1[0]/n;
  v1[1] = v1[1]/n;
  v1[2] = v1[2]/n;
}

#endif
